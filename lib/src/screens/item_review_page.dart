import 'dart:ui';

import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/star_rating_bar.dart';
import 'package:flutter/material.dart';

class ItemReviewPage extends StatefulWidget {
  int foodId;
  int userId;
  ItemReviewPage({this.foodId, this.userId});
  //const ItemReviewPage({Key? key}) : super(key: key);

  @override
  _ItemReviewPageState createState() => _ItemReviewPageState();
}

class _ItemReviewPageState extends State<ItemReviewPage> {
  TextEditingController reviewTextEditingController =
      new TextEditingController();
  double ratingStar = 0;
  bool isLoading = false;
  void submitReview() {
    cancelAlertBoxFun(
        context: context,
        msg: "Do you wish to Submit the Review?",
        titlePadding: 3,
        contentPadding: 20,
        insetPadding: 2.7,
        onPressedNo: () {
          setState(() {
            pop(context);
          });
        },
        onPressedYes: () {
          setState(() {});
          pop(context);
        });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Constants.kitGradients[0],
        appBar: AppBar(
          backgroundColor: Constants.kitGradients[0],
          leading: GestureDetector(
            onTap: () {
              pop(context);
            },
            child: Icon(Icons.arrow_back, color: Constants.kitGradients[5]),
          ),
          title: Text(
            "Review Product",
            style: TextStyle(
              color: Constants.kitGradients[5],
              fontWeight: FontWeight.w500,
              fontSize: 20,
            ),
          ),
        ),
        body: SingleChildScrollView(
            child: Container(
                width: screenWidth(context, dividedBy: 1),
                padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 25),
                  vertical: screenHeight(context, dividedBy: 60),
                ),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      Center(
                        child: Text(
                          "Rate your Review",
                          style: TextStyle(
                            color: Constants.kitGradients[2],
                            fontWeight: FontWeight.w500,
                            fontSize: 20,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      Center(
                        child: StarRatingBar(
                          rating: ratingStar,
                          size: 30,
                          onRatingChanged: (rating) =>
                              setState(() => ratingStar = rating),
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      Text(
                        "Write your Review",
                        style: TextStyle(
                          color: Constants.kitGradients[5],
                          fontWeight: FontWeight.w500,
                          fontSize: 17,
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      Container(
                        child: TextField(
                          controller: reviewTextEditingController,
                          cursorColor: Constants.kitGradients[5],
                          keyboardType: TextInputType.multiline,
                          maxLines: 6,
                          obscureText: false,
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Constants.kitGradients[5],
                            fontFamily: "Prompt-Light",
                          ),
                          //readOnly: widget.readOnly,
                          //onChanged:(){},
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: screenWidth(context, dividedBy: 40),
                                vertical: screenHeight(context, dividedBy: 80)),
                            hintText: "Type something",
                            hintStyle: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w400,
                              color: Constants.kitGradients[5],
                              fontFamily: "Prompt-Light",
                            ),
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Constants.kitGradients[2], width: 1),
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Constants.kitGradients[2], width: 1),
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      Container(
                        width: screenWidth(context, dividedBy: 1),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            BuildButton(
                              title: "Submit",
                              onPressed: () {
                                if (reviewTextEditingController != null &&
                                    ratingStar != null) {
                                  submitReview();
                                } else {
                                  showToast("Please fill all the details");
                                }
                              },
                            ),
                          ],
                        ),
                      ),
                    ]))));
  }
}
