import 'package:app_template/src/app.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/hive.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/build_dropdown_categories.dart';
import 'package:app_template/src/widgets/heading_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  String language = "";
  String code = "";

  List<String> languageList = [
    "Arabic ",
    "English",
  ];

  bool notifications;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 12)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AppBarDelivery(
              isWhite: false,
              color: Constants.kitGradients[0],
              title: "Settings",
              leftIcon: Icons.arrow_back_ios,
              onPressedLeftIcon: () {
                pop(context);
              },
            ),
          ],
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 15)),
        child: Column(
          children: [
            Row(
              children: [
                HeadingWidget(
                  title: "Language",
                ),
              ],
            ),
            DropDownFormField(
              hintText: true,
              dropDownList: languageList,
              fontFamily: "OpenSansRegular",
              textBoxWidth: 1.36,
              boxHeight: 12,
              boxWidth: 1.2,
              dropDownValue: language,
              hintFontFamily: "OpenSansRegular",
              underline: true,
              centreAlignText: true,
              onClicked: (value) {
                if (value == "English") {
                  language = value;
                  code = "en";
                } else {
                  language = value;
                  code = "ar";
                }

                ObjectFactory().appHive.putLanguage(code);
                print(ObjectFactory().appHive.getLanguage().toString());

                setState(() {
                  language = value;
                });
                MyApp.setLocale(context, Locale(code));
              },
              hintPadding: screenWidth(context),
              title: "Langauge",
            ),
            Container(
              width: screenWidth(context, dividedBy: 1),
              child: Row(
                children: [
                  HeadingWidget(title: "Notifications"),
                  Spacer(
                    flex: 1,
                  ),
                  Switch(
                    value: notifications,
                    activeColor: Constants.kitGradients[6],
                    inactiveThumbColor: Constants.kitGradients[0],
                    onChanged: (isOn) {
                      setState(() {
                        notifications = isOn;
                      });

                      print(notifications);
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
