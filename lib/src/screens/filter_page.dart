import 'package:app_template/src/screens/search_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/address_heading_widget.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/build_dropdown_categories.dart';
import 'package:app_template/src/widgets/heading_widget.dart';
import 'package:app_template/src/widgets/restuarant_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FilterPage extends StatefulWidget {
  @override
  _FilterPageState createState() => _FilterPageState();
}

class _FilterPageState extends State<FilterPage> {
  RangeValues _currentRangeValues = const RangeValues(0, 5000);
  RangeValues _ratingRangeValues = const RangeValues(0, 5);
  List<String> categories = [
    "Arabic",
    "Cheness",
    "kerala",
    "Thailand",
    "Veg",
    "No-Veg"
  ];
  String values = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 13)),
          child: AppBarDelivery(
            title: "Filter ",
            elevation: 0,
            onPressedLeftIcon: () {
              pop(context);
            },
            color: Constants.kitGradients[0],
            leftIcon: Icons.arrow_back_ios,
            leftIconTrue: true,
            isWhite: true,
          )),
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 20)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              children: [
                AddressHeadingWidget(
                  title: "Filter By Price",
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  HeadingWidget(
                    title: _currentRangeValues.start.round().toString(),
                  ),
                  HeadingWidget(
                    title: _currentRangeValues.end.round().toString(),
                  ),
                ],
              ),
            ),
            RangeSlider(
                divisions: 10,
                min: 0,
                max: 5000,
                activeColor: Constants.kitGradients[2].withOpacity(0.6),
                inactiveColor: Constants.kitGradients[0],
                values: _currentRangeValues,
                labels: RangeLabels(
                  _currentRangeValues.start.round().toString(),
                  _currentRangeValues.end.round().toString(),
                ),
                onChanged: (RangeValues values) {
                  setState(() {
                    _currentRangeValues = values;
                  });
                }),
            Row(
              children: [
                AddressHeadingWidget(
                  title: "Filter By Rating",
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  HeadingWidget(
                    title: _ratingRangeValues.start.round().toString(),
                  ),
                  HeadingWidget(
                    title: _ratingRangeValues.end.round().toString(),
                  ),
                ],
              ),
            ),
            RangeSlider(
                divisions: 5,
                min: 0,
                max: 5,
                activeColor: Constants.kitGradients[2].withOpacity(0.6),
                inactiveColor: Constants.kitGradients[0],
                values: _ratingRangeValues,
                labels: RangeLabels(
                  _ratingRangeValues.start.round().toString(),
                  _ratingRangeValues.end.round().toString(),
                ),
                onChanged: (RangeValues values) {
                  setState(() {
                    _ratingRangeValues = values;
                  });
                }),
            SizedBox(
              height: screenHeight(context, dividedBy: 15),
            ),
            DropDownFormField(
              hintText: true,
              dropDownList: categories,
              fontFamily: "OpenSansRegular",
              textBoxWidth: 1.36,
              boxHeight: 12,
              boxWidth: 1.2,
              dropDownValue: values,
              hintFontFamily: "OpenSansRegular",
              underline: true,
              onClicked: (value) {
                setState(() {
                  values = value;
                });
              },
              hintPadding: screenWidth(context, dividedBy: 3),
              title: "Filter By Categories",
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 3.5),
            ),
            RestaurantButton(
              title: "CONTINUE",
              onPressed: () {
                push(context, SearchPage());
              },
            ),
          ],
        ),
      ),
    );
  }
}
