import 'dart:async';

import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class TestMapPolyline extends StatefulWidget {
  @override
  _TestMapPolylineState createState() => _TestMapPolylineState();
}

class _TestMapPolylineState extends State<TestMapPolyline> {
  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> _markers = {};
  Set<Polyline> _polylines = {};
  List<LatLng> polylineCoordinates = [];
  PolylinePoints polylinePoints = PolylinePoints();

  final double CAMERA_ZOOM = 13;
  final double CAMERA_TILT = 0;
  final double CAMERA_BEARING = 30;
  PointLatLng SOURCE_LOCATION = PointLatLng(11.8404, 76.2030);
  PointLatLng DEST_LOCATION = PointLatLng(11.8544, 76.2030);
  LatLng SOURCE_LOCATION_LAT = LatLng(11.8404, 76.2030);
  LatLng DEST_LOCATION_LAT = LatLng(11.8544, 76.2030);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GoogleMap(
        //that needs a list<Polyline>
        polylines: _polylines,
        trafficEnabled: true,
        markers: _markers,
        onMapCreated: _onMapCreated,

        initialCameraPosition: CameraPosition(
            zoom: CAMERA_ZOOM,
            bearing: CAMERA_BEARING,
            tilt: CAMERA_TILT,
            target: SOURCE_LOCATION_LAT),
        mapType: MapType.normal,
      ),
    );
  }

  void _onMapCreated(GoogleMapController controllerParam) {
    print("Map Created");
    setMapPins();
    // setPolylines();
  }

  setPolylines() async {
    PolylineResult result = await polylinePoints?.getRouteBetweenCoordinates(
        Constants.GoogleAPIKEY, SOURCE_LOCATION, DEST_LOCATION);
    print("PolyLines");
    print(result.errorMessage);
    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        print(point.latitude.toString());
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }
    setState(() {
      // create a Polyline instance
      // with an id, an RGB color and the list of LatLng pairs
      Polyline polyline = Polyline(
          polylineId: PolylineId("Poly"),
          color: Colors.red,
          points: polylineCoordinates);

      _polylines.add(polyline);
    });
  }

  void setMapPins() {
    setState(() {
      // source pin
      _markers.add(Marker(
        markerId: MarkerId("sourcePin"),
        position: SOURCE_LOCATION_LAT,
      ));
      // destination pin
      _markers.add(Marker(
        markerId: MarkerId("destPin"),
        position: DEST_LOCATION_LAT,
      ));
    });
  }
}
