import 'package:app_template/src/models/calorie_model.dart';
import 'package:app_template/src/screens/item_review_page.dart';
import 'package:app_template/src/screens/review_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/add_athelete_number_widget.dart';
import 'package:app_template/src/widgets/address_heading_widget.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/delivery_icon_widget.dart';
import 'package:app_template/src/widgets/location_preference_icon.dart';
import 'package:app_template/src/widgets/price_widget.dart';
import 'package:app_template/src/widgets/products_images.dart';
import 'package:app_template/src/widgets/profile_icon_widget.dart';
import 'package:app_template/src/widgets/rating_widget.dart';
import 'package:app_template/src/widgets/restuarant_button.dart';
import 'package:app_template/src/widgets/review_button.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class ProductDetailPage extends StatefulWidget {
  @override
  _ProductDetailPageState createState() => _ProductDetailPageState();
}

class _ProductDetailPageState extends State<ProductDetailPage> {
  int currentIndex = 0;
  int count = 0;
  bool moreReview = false;
  List<String> foodImageDetails = [
    "assets/images/product-05.jpg",
    "assets/images/product-02.jpg",
    "assets/images/product-03.jpg",
    "assets/images/product-04.jpg",
    "assets/images/product-05.jpg",
    "assets/images/product-04.jpg"
  ];
  List<CalorieModel> data = [
    CalorieModel(
        barColor: charts.ColorUtil.fromDartColor(Colors.green),
        items: "Calorie",
        percentage: 100),
    CalorieModel(
      barColor: charts.ColorUtil.fromDartColor(Colors.red),
      items: "Fat",
      percentage: 20,
    ),
    CalorieModel(
        barColor: charts.ColorUtil.fromDartColor(Colors.white),
        items: "",
        percentage: 20),
    CalorieModel(
        barColor: charts.ColorUtil.fromDartColor(Colors.white),
        items: "",
        percentage: 20),
    CalorieModel(
        barColor: charts.ColorUtil.fromDartColor(Colors.white),
        items: "",
        percentage: 20)
  ];

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Constants.kitGradients[0],
        body: Column(
          children: [
            AppBarDelivery(
              title: "",
              elevation: 0,
              onPressedLeftIcon: () {
                pop(context);
              },
              color: Constants.kitGradients[0],
              leftIcon: Icons.arrow_back_ios,
              leftIconTrue: true,
              isWhite: true,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 30)),
              child: Container(
                height: screenHeight(context, dividedBy: 1.118),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      CarouselSlider(
                        options: CarouselOptions(
                            height: screenHeight(context, dividedBy: 2.3),
                            onPageChanged: (index, reason) {
                              currentIndex = index;
                              print(currentIndex);
                              setState(() {});
                            },
                            reverse: false,
                            initialPage: 0,
                            viewportFraction: 1,
                            autoPlay: false),
                        items: foodImageDetails.map((i) {
                          return Builder(builder: (BuildContext context) {
                            return Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal:
                                      screenWidth(context, dividedBy: 60)),
                              child: ProductImages(
                                imgListLength: foodImageDetails.length,
                                onPressed: () {},
                                images: i,
                                currentIndex: currentIndex,
                              ),
                            );
                          });
                        }).toList(),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: screenWidth(context, dividedBy: 100)),
                        child: Column(children: [
                          SizedBox(
                            height: screenHeight(context, dividedBy: 20),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.radio_button_checked,
                                    size: 20,
                                    color: Colors.red,
                                  ),
                                  SizedBox(
                                    width: screenWidth(context, dividedBy: 20),
                                  ),
                                  Container(
                                    width: screenWidth(context, dividedBy: 2),
                                    child: Text(
                                      "Salmon Pizza",
                                      style: TextStyle(
                                          color: Constants.kitGradients[5],
                                          fontSize: screenWidth(context,
                                              dividedBy: 20),
                                          fontFamily: "PrompLight"),
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                  height: screenHeight(context, dividedBy: 23),
                                  width: screenWidth(context, dividedBy: 5),
                                  child: ReviewButton(
                                    title: "Add",
                                    onPressed: () {},
                                  ))
                            ],
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 50),
                          ),
                          Row(
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal:
                                        screenWidth(context, dividedBy: 10)),
                                child: PriceWidget(
                                  price: "567",
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 50),
                          ),
                          Text(
                            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                            style: TextStyle(
                                color: Colors.grey,
                                fontFamily: "OpenSansRegular",
                                fontSize: 14),
                            textAlign: TextAlign.justify,
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 50),
                          ),
                          Row(
                            children: [
                              AddressHeadingWidget(
                                title: "Restaurant",
                              ),
                            ],
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 50),
                          ),
                          Row(
                            children: [
                              Text(
                                "Kabani",
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontFamily: "OpenSansRegular",
                                    fontSize: 14),
                                textAlign: TextAlign.justify,
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                "Suthan bathery, Wayanad",
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontFamily: "OpenSansRegular",
                                    fontSize: 12),
                                textAlign: TextAlign.justify,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 30),
                          ),
                          Row(
                            children: [
                              ReviewButton(
                                onPressed: () {
                                  push(context, ReviewPage());
                                },
                                title: "Reviews",
                              )
                            ],
                          ),
                          // Row(
                          //   children: [
                          //     AddressHeadingWidget(
                          //       title: "Reviews",
                          //     ),
                          //   ],
                          // ),
                          //
                          SizedBox(
                            height: screenHeight(context, dividedBy: 30),
                          ),
                          // ListView.builder(
                          //     shrinkWrap: true,
                          //     physics: NeverScrollableScrollPhysics(),
                          //     padding: EdgeInsets.zero,
                          //     itemCount: moreReview == true
                          //         ? 3
                          //         : 3 > 4
                          //             ? 3
                          //             : 2,
                          //     itemBuilder: (BuildContext context, int index) {
                          //       return ReviewWidget(
                          //         body: "Nice Kerala Food",
                          //         reviewTitle: "AwsomeFood",
                          //       );
                          //     })
                        ]),
                      ),
                      Row(
                        children: [
                          AddressHeadingWidget(
                            title: "Calories",
                          ),
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 40),
                      ),
                      Row(
                        children: [
                          Container(
                            height: screenHeight(context, dividedBy: 4),
                            width: (screenWidth(context, dividedBy: 1.1)),
                            child: charts.BarChart(
                              _getSeriesData(),
                              vertical: true,
                              defaultRenderer: charts.BarRendererConfig(
                                  strokeWidthPx: 10,
                                  maxBarWidthPx: 30,
                                  minBarLengthPx: 30,
                                  stackedBarPaddingPx: 100),
                              domainAxis: charts.OrdinalAxisSpec(
                                  renderSpec: charts.SmallTickRendererSpec(
                                      labelRotation: 60)),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ));
  }

  void incrementFunction() {
    if (count <= 10) {
      setState(() {
        count = count + 1;
      });
    } else {
      setState(() {});
    }
  }

  void decrementFunction() {
    if (count > 0) {
      setState(() {
        count = count - 1;
      });
    } else {
      setState(() {});
    }
  }

  _getSeriesData() {
    List<charts.Series<CalorieModel, String>> series = [
      charts.Series(
          id: "Population",
          data: data,
          domainFn: (CalorieModel series, _) => series.items.toString(),
          measureFn: (CalorieModel series, _) => series.percentage,
          colorFn: (CalorieModel series, _) => series.barColor)
    ];
    return series;
  }
}
