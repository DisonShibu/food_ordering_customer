import 'package:app_template/src/screens/cart_page.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/search_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/home_tab_bar.dart';
import 'package:app_template/src/widgets/list_tile_myorders.dart';
import 'package:app_template/src/widgets/restaurant_tile.dart';
import 'package:app_template/src/widgets/review_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyOrdersPage extends StatefulWidget {
  @override
  _MyOrdersPageState createState() => _MyOrdersPageState();
}

class _MyOrdersPageState extends State<MyOrdersPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[0],
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: HomeTabBar(
          currentIndex: 2,
          onTapHome: () {
            push(context, HomePage());
          },
          onTapSearch: () {
            push(context, SearchPage());
          },
          onTapOrder: () {},
          onTapCart: () {
            push(context, CartPage());
          }),
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 13)),
          child: AppBarDelivery(
            title: "My Orders",
            elevation: 3,
            onPressedLeftIcon: () {
              pop(context);
            },
            color: Constants.kitGradients[0],
            leftIcon: Icons.arrow_back_ios,
            leftIconTrue: true,
            isWhite: true,
          )),
      body: Padding(
        padding: EdgeInsets.only(top: screenWidth(context, dividedBy: 20)),
        child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            physics: AlwaysScrollableScrollPhysics(),
            addAutomaticKeepAlives: true,
            padding: EdgeInsets.zero,
            itemCount: 2,
            itemBuilder: (BuildContext context, int index) {
              return ListTileMyOrders(
                foodImage:
                    "https://images.saymedia-content.com/.image/t_share/MTc0Mzc4NTY2MjIwNjUzOTI4/best-burger-restaurant-names.jpg",
                name: "Burger",
                address: "Kakkanattu (H),Seethamount (P.0),Pulpally",
                orderNum: "ORD567",
                price: "567",
                cancelled: true,
                cancelledStatus: "Delivered",
              );
            }),
      ),
    );
  }
}
