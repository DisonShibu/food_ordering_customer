import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/my_account.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/build_dropdown_categories.dart';
import 'package:app_template/src/widgets/description_textfield.dart';
import 'package:app_template/src/widgets/restuarant_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProfileUpdatePage extends StatefulWidget {
  @override
  _ProfileUpdatePageState createState() => _ProfileUpdatePageState();
}

class _ProfileUpdatePageState extends State<ProfileUpdatePage> {
  String code = "+91";
  TextEditingController nameTextEditingController = new TextEditingController();
  TextEditingController phoneTextEditingController =
      new TextEditingController();
  TextEditingController emailTextEditingController =
      new TextEditingController();

  List<String> categories = [
    "+971",
    "+966",
    "+974",
    "+968",
  ];
  String values = "+971";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Constants.kitGradients[0],
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                height: screenHeight(context, dividedBy: 2),
                color: Constants.kitGradients[2],
                alignment: Alignment.topCenter,
                child: Column(
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 10),
                    ),
                    Image.asset(
                      "assets/icons/delivery_app_icon.jpg",
                      height: screenHeight(context, dividedBy: 7),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Column(
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 4),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 40)),
                child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  child: Container(
                    height: screenHeight(context, dividedBy: 1.7),
                    decoration: BoxDecoration(
                        color: Constants.kitGradients[3]
                            .withOpacity(0.9)
                            .withOpacity(0.9),
                        borderRadius: BorderRadius.circular(30)),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: screenHeight(context, dividedBy: 30),
                          horizontal: screenWidth(context, dividedBy: 17)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            children: [
                              Text(
                                "Phone Number",
                                style: TextStyle(
                                  color: Constants.kitGradients[0],
                                  fontWeight: FontWeight.w400,
                                  fontSize: 19,
                                  fontFamily: "OswaldRegular",
                                ),
                              ),
                            ],
                          ),
                          Spacer(
                            flex: 1,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                height: screenHeight(context, dividedBy: 16),
                                decoration: BoxDecoration(
                                    color: Constants.kitGradients[0],
                                    borderRadius: BorderRadius.circular(20),
                                    border: Border.all(
                                        color: Constants.kitGradients[0],
                                        width: 0.0)),
                                child: Center(
                                  child: DropDownFormField(
                                    hintText: true,
                                    dropDownList: categories,
                                    fontFamily: "OpenSansRegular",
                                    textBoxWidth: 6.3,
                                    boxHeight: 17,
                                    boxWidth: 6.2,
                                    dropDownValue: values,
                                    hintFontFamily: "OpenSansRegular",
                                    underLineFalse: false,
                                    centreAlignText: true,
                                    onClicked: (value) {
                                      setState(() {
                                        values = value;
                                      });
                                    },
                                    hintPadding: 360,
                                    title: "91",
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: screenHeight(context, dividedBy: 100),
                              ),
                              GestureDetector(
                                onTap: () {},
                                child: Container(
                                  width: screenWidth(context, dividedBy: 1.59),
                                  child: DescriptionTextField(
                                    hintText: "PhoneNumber",
                                    textEditingController:
                                        phoneTextEditingController,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Spacer(
                            flex: 1,
                          ),
                          Row(
                            children: [
                              Text(
                                "UserName",
                                style: TextStyle(
                                  color: Constants.kitGradients[0],
                                  fontWeight: FontWeight.w400,
                                  fontSize: 19,
                                  fontFamily: "OswaldRegular",
                                ),
                              ),
                            ],
                          ),
                          Spacer(
                            flex: 1,
                          ),
                          Container(
                            width: screenWidth(context, dividedBy: 1.27),
                            child: DescriptionTextField(
                              onPressed: () {},
                              hintText: "UserName",
                              suffixIcon: null,
                              phoneNumber: true,
                              textEditingController: nameTextEditingController,
                            ),
                          ),
                          Spacer(
                            flex: 1,
                          ),
                          Row(
                            children: [
                              Text(
                                "Email",
                                style: TextStyle(
                                  color: Constants.kitGradients[0],
                                  fontWeight: FontWeight.w400,
                                  fontSize: 19,
                                  fontFamily: "OswaldRegular",
                                ),
                              ),
                            ],
                          ),
                          Spacer(
                            flex: 1,
                          ),
                          Container(
                            child: DescriptionTextField(
                              hintText: "Email",
                              phoneNumber: true,
                              textEditingController: emailTextEditingController,
                            ),
                          ),
                          Spacer(
                            flex: 6,
                          ),
                          Container(
                            child: RestaurantButton(
                              title: "Update Details",
                              onPressed: () {
                                push(context, ProfileUpdatePage());
                                if (emailTextEditingController != null ||
                                    phoneTextEditingController != null ||
                                    nameTextEditingController != null) {
                                  push(context, MyAccountPage());
                                  showToast("Details updated");
                                } else {
                                  showToast("No data is Entered");
                                }
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
