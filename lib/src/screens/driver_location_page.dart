import 'dart:io';

import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/audio_recoring_widget.dart';
import 'package:app_template/src/widgets/driver_name_widget.dart';
import 'package:app_template/src/widgets/list_tile_food_count.dart';
import 'package:app_template/src/widgets/saved_address_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class DriverLocationPage extends StatefulWidget {
  @override
  _DriverLocationPageState createState() => _DriverLocationPageState();
}

class _DriverLocationPageState extends State<DriverLocationPage> {
  File audioFile;
  PDFDocument document;
  String diverPhoneNumber = "";
  String restaurantPhoneNumber = "";

  Future<void> loadPdf() async {
    document = await PDFDocument.fromURL(
        'http://www.africau.edu/images/default/sample.pdf');
  }

  Future<void> _callNumber(phoneNumber) async {
    String url = "tel://" + phoneNumber;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not call $phoneNumber';
    }
  }

  @override
  void initState() {
    loadPdf();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: screenHeight(context, dividedBy: 1),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: screenHeight(context, dividedBy: 4),
                width: screenWidth(context, dividedBy: 1),
                color: Constants.kitGradients[2],
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      Row(
                        children: [
                          Spacer(
                            flex: 1,
                          ),
                          GestureDetector(
                            onTap: () {
                              pop(context);
                            },
                            child: Icon(
                              Icons.arrow_back_ios,
                              color: Colors.white,
                              size: 20,
                            ),
                          ),
                          Spacer(
                            flex: 3,
                          ),
                          Spacer(
                            flex: 5,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      Text(
                        "Preparing your order",
                        style: TextStyle(
                            color: Constants.kitGradients[0],
                            fontWeight: FontWeight.bold,
                            fontSize: 19,
                            fontFamily: "OpenSansRegular"),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 700),
                      ),
                      Text(
                        "Arriving in 20 minutes",
                        style: TextStyle(
                            color: Constants.kitGradients[0],
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                            fontFamily: "OpenSansRegular"),
                      ),
                    ]),
              ),
              Container(
                height: screenHeight(context, dividedBy: 2),
                color: Constants.kitGradients[1],
              ),
              Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 80),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 20)),
                    child: Container(
                      width: screenWidth(context, dividedBy: 1),
                      height: screenHeight(context, dividedBy: 8),
                      child: DriverNameWidget(
                        title: "Hi, I am Asif,your vallet",
                        driverMessage: "I am on your way to Home",
                        onPressed: () {
                          print("widgetPrseed");
                        },
                        profilePic: "assets/images/userprofile.png",
                        onPressedLeftIcon: () {},
                        onPressedRightIcon: () {
                          _callNumber("7560886107");
                        },
                        onPressedText: () {},
                        driverMessageTrue: true,
                        containerWidth: 2,
                        chatBox: true,
                        profileImageTrue: true,
                        iconRight: Icons.call,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                  AudioRecordingWidget(
                    onChanged: (value) {
                      audioFile = value;
                    },
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 20)),
                    child: DriverNameWidget(
                      containerWidth: 1.5,
                      profileImageTrue: false,
                      leftIconTrue: true,
                      onPressedLeftIcon: () {},
                      iconLeft: Icons.medical_services_outlined,
                      onPressedRightIcon: () {
                        _callNumber("7560886107");
                      },
                      onPressedText: () {
                        showBottomSheet(context);
                      },
                      title: "I have been partially vaccinated ",
                      richTextSubHeading: "View Certificate",
                      driverMessageTrue: false,
                      richTextTrue: true,
                      rightIconTrue: false,
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 20)),
                    child: DriverNameWidget(
                      containerWidth: 1.5,
                      profileImageTrue: false,
                      leftIconTrue: true,
                      onPressedLeftIcon: () {},
                      iconLeft: Icons.title,
                      onPressedRightIcon: () {},
                      onPressedText: () {},
                      title:
                          "My temprature was 98.6 \u2109 (normal) at   6.22 pm",
                      richTextSubHeading: "",
                      driverMessageTrue: false,
                      richTextTrue: true,
                      rightIconTrue: false,
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 20)),
                    child: Container(
                      height: screenHeight(context, dividedBy: 7),
                      child: DriverNameWidget(
                        profilePic: "assets/images/hotel_name.png",
                        title: "Mothers veg Plaza",
                        richTextSubHeading: "\n palyam,trivandrum",
                        driverMessage: "Your Order is Preparing",
                        onPressed: () {},
                        onPressedLeftIcon: () {},
                        onPressedRightIcon: () {
                          _callNumber("7560886107");
                        },
                        onPressedText: () {},
                        driverMessageTrue: true,
                        containerWidth: 2,
                        chatBox: true,
                        profileImageTrue: true,
                        iconRight: Icons.call,
                      ),
                    ),
                  ),
                  SavedAddressBox(
                    center: true,
                    svgIconTrue: true,
                    image: "assets/images/userprofile.png",
                    heading: "Dison Shibu",
                    subHeading: "Kakkanattu Pulpally",
                    thirdLineTrue: false,
                    onPressedAddress: () {},
                  ),
                  ListView.builder(
                      padding: EdgeInsets.zero,
                      itemCount: 3,
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, int index) {
                        return ListTileFoodCount(
                          foodcount: "1",
                          foodname: "pizza",
                        );
                      }),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 80),
                  ),
                  Divider(
                    color: Colors.black45,
                  ),
                  SavedAddressBox(
                    center: true,
                    svgIconTrue: true,
                    image: "assets/images/help.png",
                    heading: "Cancel Your Order ",
                    subHeading:
                        "Cancel your order before driver picks up the food.",
                    thirdLineTrue: true,
                    thirdLine: "Click to cancel",
                    onPressedAddress: () {
                      cancelAlertBoxFun(
                          contentPadding: 80,
                          context: context,
                          insetPadding: 3,
                          msg: "Do you wish to cancel your Order ?",
                          onPressedNo: () {},
                          onPressedYes: () {
                            setState(() {});
                          },
                          titlePadding: 0);
                    },
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void showBottomSheet(BuildContext context) {
    showModalBottomSheet(
        isScrollControlled: false,
        enableDrag: false,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        context: context,
        builder: (context) {
          return Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
              ),
              child: PDFViewer(
                pickerButtonColor: Constants.kitGradients[2],
                maxScale: 7,
                document: document,
                scrollDirection: Axis.vertical,
              ));
        });
  }
}
