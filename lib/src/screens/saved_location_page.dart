import 'package:app_template/src/models/hive_db_models/saved_location_model.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/search_page.dart';
import 'package:app_template/src/state/app_state.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/location_selection_box.dart';
import 'package:app_template/src/widgets/address_adding_page.dart';
import 'package:app_template/src/widgets/saved_address_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:j_location_picker/j_location_picker.dart';

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:j_location_picker/generated/l10n.dart' as location_picker;
import 'package:provider/provider.dart';

class SavedLocationPage extends StatefulWidget {
  @override
  _SavedLocationPageState createState() => _SavedLocationPageState();
}

class _SavedLocationPageState extends State<SavedLocationPage> {
  LocationResult pickedLocation;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            child: AppBarDelivery(
              title: "",
              elevation: 0,
              onPressedLeftIcon: () {
                pop(context);
              },
              color: Constants.kitGradients[0],
              leftIcon: Icons.arrow_back_ios,
              leftIconTrue: true,
              isWhite: true,
            ),
          ),
          Container(
              color: Constants.kitGradients[0],
              child: LocationSelectionBox(
                padding: 20,
                boxWidth: 1.3,
                colorWhite: false,
                subHeadingTrue: true,
                heading: "Search For location",
                subHeading: "Search for area, street name",
                icon: Icons.location_on_outlined,
                dividerTrue: true,
                onPressed: () async {
                  LocationResult result = await showLocationPicker(
                      context, Constants.GoogleAPIKEY,
                      myLocationButtonEnabled: true,
                      desiredAccuracy: LocationAccuracy.best,
                      automaticallyAnimateToCurrentLocation: true,
                      countries: ["AE"],
                      language: ObjectFactory().appHive.getLanguage() == null
                          ? "en"
                          : ObjectFactory().appHive.getLanguage());
                  print("result = $result");
                  if (result != null) {
                    push(
                        context,
                        AddressAddingPage(
                          location: result.address,
                          latLng: LatLng(
                              result.latLng.latitude, result.latLng.longitude),
                        ));
                  }
                },
              )),
          Container(
              color: Constants.kitGradients[0],
              child: LocationSelectionBox(
                subHeadingTrue: true,
                boxWidth: 1.3,
                padding: 20,
                dividerTrue: true,
                heading: "Current Location",
                subHeading: "Enable Location Services",
                icon: Icons.my_location,
                onPressed: () async {
                  LocationResult result = await showLocationPicker(
                      context, Constants.GoogleAPIKEY,
                      myLocationButtonEnabled: true,
                      desiredAccuracy: LocationAccuracy.best,
                      automaticallyAnimateToCurrentLocation: true,
                      countries: ["AE"],
                      language: ObjectFactory().appHive.getLanguage() == null
                          ? "en"
                          : ObjectFactory().appHive.getLanguage());
                  print("result = $result");
                  if (result != null) {
                    push(
                        context,
                        AddressAddingPage(
                          location: result.address,
                          latLng: LatLng(
                              result.latLng.latitude, result.latLng.longitude),
                        ));
                  }
                },
              )),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 20)),
            child: Padding(
              padding: EdgeInsets.only(
                  top: screenHeight(context, dividedBy: 80),
                  bottom: screenHeight(context, dividedBy: 50)),
              child: Row(
                children: [
                  Text(
                    "SAVED ADDRESS",
                    style: TextStyle(
                      color: Colors.black54,
                      fontWeight: FontWeight.w800,
                      fontSize: 16,
                      fontFamily: "PrompLight",
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 20)),
            child: ListView.builder(
                padding: EdgeInsets.zero,
                itemCount: 3,
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  return SavedAddressBox(
                    icon: Icons.home_outlined,
                    heading: "Home",
                    subHeading: "Kazhakuttom,Kerala,India",
                    onPressedAddress: () {
                      ObjectFactory().appHive.putUserLongitude("");
                      ObjectFactory().appHive.putUserLatitude("");
                      Provider.of<HomeState>(context, listen: false)
                          .locationStatus(value: true);
                      Provider.of<HomeState>(context, listen: false)
                          .locationValueStored(
                              locationCoordinates: Position(
                                  longitude: 67.3737, latitude: 67.885),
                              value: "Kazhakutton");



                      pop(context);
                    },
                  );
                }),
          )
        ],
      ),
    );
  }
}
