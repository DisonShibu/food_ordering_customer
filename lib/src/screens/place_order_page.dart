import 'package:app_template/src/screens/driver_location_page.dart';
import 'package:app_template/src/screens/search_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/build_icon_button.dart';
import 'package:app_template/src/widgets/label_value_widget.dart';
import 'package:app_template/src/widgets/location_selection_box.dart';
import 'package:app_template/src/widgets/place_order_food_tile.dart';
import 'package:app_template/src/widgets/price_widget.dart';
import 'package:app_template/src/widgets/profile_page_information_tile.dart';
import 'package:app_template/src/widgets/saved_address_widget.dart';
import 'package:flutter/material.dart';

class PlaceOrderPage extends StatefulWidget {
  @override
  _PlaceOrderPageState createState() => _PlaceOrderPageState();
}

class _PlaceOrderPageState extends State<PlaceOrderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Constants.kitGradients[0],
        floatingActionButton: Container(
          width: screenWidth(context, dividedBy: 1.2),
          child: BuildIconButton(
            onPressed: () {
              push(context, DriverLocationPage());
            },
            price: "10" + " SR",
            checkOut: true,
            title: "CheckOut ",
            icon: "assets/icons/cart.svg",
            colorChange: true,
            color: Constants.kitGradients[11].withOpacity(0.8),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        appBar: PreferredSize(
            preferredSize:
                Size.fromHeight(screenHeight(context, dividedBy: 13)),
            child: AppBarDelivery(
              title: "Place Order",
              elevation: 0,
              onPressedLeftIcon: () {
                pop(context);
              },
              color: Constants.kitGradients[0],
              leftIcon: Icons.arrow_back_ios,
              leftIconTrue: true,
              isWhite: true,
            )),
        body: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 40)),
          child: Column(
            children: [
              PlaceOrderFoodTile(
                image:
                    "https://images.saymedia-content.com/.image/t_share/MTc0Mzc4NTY2MjIwNjUzOTI4/best-burger-restaurant-names.jpg",
                restaurantName: "PizzaWorld",
                foodName: "Burger",
                price: "567 " + "SR",
                quantity: 1,
              ),
              SavedAddressBox(
                icon: Icons.location_on_outlined,
                heading: "Deliver To",
                subHeading: "Kazhakuttom,Kerala,India",
                onPressedAddress: () {
                  push(context, SearchPage());
                },
              ),
              Row(
                children: [
                  Container(
                    width: screenWidth(context, dividedBy: 1.12),
                    child: ProfilePageInformationTile(
                      icon: "assets/icons/promo_coupon.png",
                      title: "Add promoCode",
                      doublePadding: 0,
                      promoCodeTrue: true,
                      isArrow: true,
                      checkBoxTrue: false,
                      onPressed: () {},
                    ),
                  ),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 40),
                  )
                ],
              ),
              Container(
                child: ProfilePageInformationTile(
                  icon: "assets/icons/cash.png",
                  title: "Cash OnDelivery",
                  doublePadding: 0,
                  promoCodeTrue: true,
                  isArrow: false,
                  checkBoxTrue: true,
                  onPressed: () {},
                  onChecked: (value) {},
                ),
              ),
              Divider(
                color: Colors.black54,
              ),
              LabelAndValueWidget(
                label: "ItemTotal",
                value: "10 " + "SR",
              ),
              LabelAndValueWidget(
                label: "ServiceCharge",
                value: "20 " + "SR",
              ),
              LabelAndValueWidget(
                label: "DeliveryCharge",
                value: "30 " + "SR",
              ),
              LabelAndValueWidget(
                label: "GrandTotal",
                value: "600 " + "SR",
              ),
              Divider(
                color: Colors.black54,
              ),
            ],
          ),
        ));
  }
}
