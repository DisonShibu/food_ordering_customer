import 'package:app_template/src/models/hive_db_models/cart_page_hive_models.dart';
import 'package:app_template/src/screens/cart_page.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/my_orders_page.dart';
import 'package:app_template/src/screens/product_detail_page.dart';
import 'package:app_template/src/screens/search_page.dart';
import 'package:app_template/src/state/app_state.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/app_bar_home_page.dart';
import 'package:app_template/src/widgets/category_widget.dart';
import 'package:app_template/src/widgets/delivery_icon_widget.dart';
import 'package:app_template/src/widgets/heading_widget.dart';
import 'package:app_template/src/widgets/home_page_offer.dart';
import 'package:app_template/src/widgets/home_tab_bar.dart';
import 'package:app_template/src/widgets/search_bar.dart';
import 'package:app_template/src/widgets/search_result_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class CategoryProductPage extends StatefulWidget {
  @override
  _CategoryProductPageState createState() => _CategoryProductPageState();
}

class _CategoryProductPageState extends State<CategoryProductPage> {
  TextEditingController searchTextEditingController =
      new TextEditingController();
  List<String> foodImageDetails = [
    "assets/images/product-05.jpg",
    "assets/images/product-02.jpg",
    "assets/images/product-03.jpg",
    "assets/images/product-04.jpg",
    "assets/images/product-05.jpg",
    "assets/images/product-04.jpg"
  ];

  int foodIdCartPage;

  @override
  Widget build(BuildContext context) {
    var addCart = Provider.of<HomeState>(context, listen: false);
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: HomeTabBar(
            currentIndex: 5,
            onTapHome: () {
              push(context, HomePage());
            },
            onTapSearch: () {
              push(context, SearchPage());
            },
            onTapOrder: () {
              push(context, MyOrdersPage());
            },
            onTapCart: () {
              push(context, CartPage());
            }),
        appBar: PreferredSize(
            preferredSize:
                Size.fromHeight(screenHeight(context, dividedBy: 13)),
            child: AppBarDelivery(
              title: "Hot Drinks",
              elevation: 3,
              onPressedLeftIcon: () {
                pop(context);
              },
              color: Constants.kitGradients[0],
              leftIcon: Icons.arrow_back_ios,
              leftIconTrue: true,
              isWhite: true,
            )),
        body: Column(
          children: [
            Container(
              height: screenHeight(context, dividedBy: 1.24),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 50),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: screenWidth(context, dividedBy: 20)),
                      child: ListView.builder(
                        itemCount: 5,
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemBuilder: (
                          context,
                          index,
                        ) {
                          return SearchResultTile(
                            onPressed: () {
                              print("Products");
                              push(context, ProductDetailPage());
                            },
                            price: "567",
                            images: "assets/images/product-02.jpg",
                            foodName: "Chicken Kebab",
                            restarauntName: "Kabani Restaurant",
                            rating: "3.5",
                            onPressedAdd: () async {
                              if (await addCart.checkSameRestaurant("1") ==
                                  true) {
                                if (await addCart.checkItem(index) == true) {
                                  await addCart.addItem(CartItems(
                                      results: ResultList(
                                    id: index,
                                    name: "Chicken Kebab",
                                  )));
                                } else {
                                  addCart.foodName != addCart.foodName;
                                  await addCart.addItem(CartItems(
                                      results: ResultList(
                                    id: index,
                                    name: "Chicken Kebab",
                                  )));
                                }

                                // print(addCart.cartItemBox.values
                                //     .toString());
                              } else {
                                cancelAlertBoxFun(
                                    contentPadding: 80,
                                    context: context,
                                    insetPadding: 3,
                                    msg:
                                        "Do you wish to remove the items from Kabani Restaurant?",
                                    onPressedNo: () {},
                                    onPressedYes: () async {
                                      await addCart.clearHive();
                                      await addCart.addItem(CartItems(
                                          results: ResultList(
                                        id: index,
                                        name: "Chicken Kebab",
                                      )));
                                      pop(context);
                                    },
                                    titlePadding: 0);
                              }
                            },
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
