import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/list_tile_myorders.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[0],
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 13)),
          child: AppBarDelivery(
            title: "Notifications",
            elevation: 3,
            onPressedLeftIcon: () {
              pop(context);
            },
            color: Constants.kitGradients[0],
            leftIcon: Icons.arrow_back_ios,
            leftIconTrue: true,
            isWhite: true,
          )),
      body: Padding(
        padding: EdgeInsets.only(top: screenWidth(context, dividedBy: 20)),
        child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            physics: AlwaysScrollableScrollPhysics(),
            addAutomaticKeepAlives: true,
            padding: EdgeInsets.zero,
            itemCount: 2,
            itemBuilder: (BuildContext context, int index) {
              return ListTileMyOrders(
                orderNumber: false,
                isPrice: false,
                doubleWidth: 5,
                doubleHeight: 10,
                foodImage:
                    "https://img.freepik.com/free-vector/push-notifications-concept-illustration_114360-4986.jpg?size=338&ext=jpg",
                name: "New Discounts",
                address: "Summer Discounts ",
                orderNum: "",
                price: "50" + "%",
                textCenter: true,
              );
            }),
      ),
    );
  }
}
