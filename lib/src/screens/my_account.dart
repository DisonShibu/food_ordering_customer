import 'package:app_template/src/screens/cart_page.dart';
import 'package:app_template/src/screens/help_and_support.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/saved_location_page.dart';
import 'package:app_template/src/screens/my_orders_page.dart';
import 'package:app_template/src/screens/driver_location_page.dart';
import 'package:app_template/src/screens/profile_update_page.dart';
import 'package:app_template/src/screens/search_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/address_heading_widget.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/delivery_icon_widget.dart';
import 'package:app_template/src/widgets/description_textfield.dart';
import 'package:app_template/src/widgets/edit_profile_widget.dart';
import 'package:app_template/src/widgets/heading_widget.dart';
import 'package:app_template/src/widgets/home_tab_bar.dart';
import 'package:app_template/src/widgets/profile_icon_widget.dart';
import 'package:app_template/src/widgets/profile_page_information_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MyAccountPage extends StatefulWidget {
  @override
  _MyAccountPageState createState() => _MyAccountPageState();
}

class _MyAccountPageState extends State<MyAccountPage> {
  TextEditingController userNameTextEditingController =
      new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          floatingActionButton: HomeTabBar(
            onTapOrder: () {
              push(context, CartPage());
            },
            onTapHome: () {
              push(context, HomePage());
            },
            onTapCart: () {
              push(context, MyOrdersPage());
            },
            onTapSearch: () {
              push(context, SearchPage());
            },
          ),
          backgroundColor: Constants.kitGradients[0],
          body: SingleChildScrollView(
            child: Stack(children: [
              // AppBarDelivery(
              //   color: Constants.kitGradients[0],
              //   title: "",
              //   icon: Icons.arrow_back_ios,
              //   onPressedLeftIcon: () {},
              //   isWhite: false,
              // ),
              Container(
                height: screenHeight(context, dividedBy: 3),
                width: screenWidth(context, dividedBy: 1),
                color: Constants.kitGradients[2],
                child: Column(
                  children: [
                    Spacer(
                      flex: 1,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 20),
                        ),
                        GestureDetector(
                          onTap: () {
                            pop(context);
                          },
                          child: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(
                          width: screenWidth(context, dividedBy: 4),
                        ),
                        Text(
                          "PROFILE",
                          style: TextStyle(
                              fontSize: screenWidth(context, dividedBy: 20),
                              color: Constants.kitGradients[0],
                              fontWeight: FontWeight.w400,
                              fontFamily: "SofiaProRegular"),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    ProfileIconWidget(
                      color: Constants.kitGradients[0],
                      phoneNumber: "7560886107",
                      imageName:
                          "https://png.pngtree.com/png-vector/20190223/ourmid/pngtree-profile-line-black-icon-png-image_691065.jpg",
                      userName: "Dison Shibu",
                    ),
                    Spacer(
                      flex: 3,
                    ),
                  ],
                ),
              ),
              Column(children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 4),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: Card(
                    child: Container(
                      width: screenWidth(context, dividedBy: 1),
                      height: screenHeight(context, dividedBy: 5.5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Spacer(
                            flex: 1,
                          ),
                          DeliveryIcon(
                            icon: Icons.delivery_dining,
                            color: Constants.kitGradients[0],
                            title: "Ongoing \n Orders",
                            onPressed: () {
                              push(context, DriverLocationPage());
                            },
                          ),
                          Spacer(
                            flex: 3,
                          ),
                          DeliveryIcon(
                            icon: Icons.fastfood,
                            color: Constants.kitGradients[0],
                            title: "My \n Orders",
                            onPressed: () {
                              push(context, MyOrdersPage());
                            },
                          ),
                          Spacer(
                            flex: 3,
                          ),
                          DeliveryIcon(
                            icon: Icons.location_on_outlined,
                            color: Constants.kitGradients[0],
                            title: "My \n Location",
                            onPressed: () {
                              push(context, SavedLocationPage());
                            },
                          ),
                          Spacer(
                            flex: 1,
                          ),
                        ],
                      ),

                      // Row(
                      //   children: [
                      //     HeadingWidget(
                      //       title: "Address",
                      //     ),
                      //     SizedBox(
                      //       width: screenWidth(context, dividedBy: 1.76),
                      //     ),
                      //     SizedBox(
                      //       width: screenWidth(context, dividedBy: 20),
                      //     ),
                      //   ],
                      // ),
                      // Container(
                      //   height: screenHeight(context, dividedBy: 5),
                      //   decoration: BoxDecoration(
                      //       color: Constants.kitGradients[7],
                      //       borderRadius: BorderRadius.circular(15)),
                      //   child: Card(
                      //     elevation: 3,
                      //     child: AddressDetailWidget(
                      //       onPressed: () {},
                      //       title: "Paris,Italy",
                      //       image: "assets/images/location.svg",
                      //     ),
                      //   ),
                      // ),
                      // Row(
                      //   children: [
                      //     HeadingWidget(
                      //       title: "Location",
                      //     ),
                      //     SizedBox(
                      //       width: screenWidth(context, dividedBy: 1.79),
                      //     ),
                      //     SizedBox(
                      //       width: screenWidth(context, dividedBy: 20),
                      //     ),
                      //   ],
                      // ),
                      // Container(
                      //   height: screenHeight(context, dividedBy: 5),
                      //   decoration: BoxDecoration(
                      //       borderRadius: BorderRadius.circular(15)),
                      //   child: Card(
                      //     elevation: 3,
                      //     child: AddressDetailWidget(
                      //       onPressed: () {},
                      //       title: "Paris,Italy",
                      //       image: "assets/images/location.svg",
                      //     ),
                      //   ),
                      // ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: Column(
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      EditProfile(
                        title: "Edit Profile",
                        onPressed: () {
                          push(context, ProfileUpdatePage());
                        },
                        icons: Icons.person_outline,
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      Card(
                        elevation: 3,
                        color: Constants.kitGradients[0],
                        child: Container(
                          height: screenHeight(context, dividedBy: 4.5),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal:
                                            screenWidth(context, dividedBy: 20),
                                        vertical: screenHeight(context,
                                            dividedBy: 50)),
                                    child: Text(
                                      "More",
                                      style: TextStyle(
                                        color: Constants.kitGradients[5],
                                        fontWeight: FontWeight.w400,
                                        fontSize: 23,
                                        fontFamily: "PrompLight",
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              ProfilePageInformationTile(
                                onPressed: () {
                                  push(context, HelpAndSupport());
                                },
                                title: "Help and support",
                                icon: "assets/images/help.png",
                                isArrow: false,
                              ),
                              ProfilePageInformationTile(
                                onPressed: () {},
                                title: "Terms",
                                icon: "assets/images/terms.png",
                                isArrow: false,
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ])
            ]),
          )),
    );
  }
}
