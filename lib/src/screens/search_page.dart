import 'package:app_template/src/models/hive_db_models/cart_page_hive_models.dart';
import 'package:app_template/src/screens/cart_page.dart';
import 'package:app_template/src/screens/filter_page.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/my_orders_page.dart';
import 'package:app_template/src/screens/product_detail_page.dart';
import 'package:app_template/src/state/app_state.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/delivery_icon_widget.dart';
import 'package:app_template/src/widgets/heading_widget.dart';
import 'package:app_template/src/widgets/home_tab_bar.dart';
import 'package:app_template/src/widgets/list_tile_myorders.dart';
import 'package:app_template/src/widgets/restaurant_tile.dart';
import 'package:app_template/src/widgets/search_bar.dart';
import 'package:app_template/src/widgets/search_result_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  TextEditingController searchTextEditingController =
      new TextEditingController();
  List<String> foodImageDetails = [
    "assets/images/product-05.jpg",
    "assets/images/product-02.jpg",
    "assets/images/product-03.jpg",
    "assets/images/product-04.jpg",
    "assets/images/product-05.jpg",
    "assets/images/product-04.jpg"
  ];

  bool restaurant = false;
  bool nonVeg = true;
  int foodIdCartPage;

  @override
  Widget build(BuildContext context) {
    var addCart = Provider.of<HomeState>(context, listen: false);
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
        floatingActionButton: HomeTabBar(
            currentIndex: 1,
            onTapHome: () {
              push(context, HomePage());
            },
            onTapSearch: () {},
            onTapOrder: () {
              push(context, MyOrdersPage());
            },
            onTapCart: () {
              push(context, CartPage());
            }),
        body: Stack(
          children: [
            Column(
              children: [
                Container(
                  height: screenHeight(context, dividedBy: 4.6),
                  width: screenWidth(context, dividedBy: 1),
                  color: Constants.kitGradients[2],
                  child: Column(
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      GestureDetector(
                        onTap: () {
                          pop(context);
                        },
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: screenWidth(context, dividedBy: 20)),
                          child: Row(
                            children: [
                              Container(
                                child: Icon(
                                  Icons.arrow_back_ios,
                                  size: screenHeight(context, dividedBy: 34),
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 40),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: screenWidth(context, dividedBy: 20)),
                        child: Row(
                          children: [
                            Expanded(
                              child: SearchBar(
                                title: "Search",
                                icon: Icons.search,
                                searchTextEditingController:
                                    searchTextEditingController,
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                nonVeg = !nonVeg;
                                setState(() {});
                              },
                              child: Container(
                                width: screenWidth(context, dividedBy: 7),
                                height: screenHeight(context, dividedBy: 15),
                                decoration: BoxDecoration(
                                    color: Colors.white.withOpacity(0.4),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Constants.kitGradients[2]
                                              .withOpacity(0.4),
                                          spreadRadius: 2.0,
                                          offset: Offset(0.4, 0.4),
                                          blurRadius: 2.0)
                                    ],
                                    borderRadius: BorderRadius.circular(5)),
                                child: Center(
                                  child: Text(
                                    nonVeg != false ? "Non-Veg" : "Veg",
                                    style: TextStyle(
                                      color: Constants.kitGradients[0],
                                      fontWeight: FontWeight.w100,
                                      fontSize:
                                          screenWidth(context, dividedBy: 23),
                                      fontFamily: "OswaldRegular",
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: screenHeight(context, dividedBy: 1.4),
                  child: SingleChildScrollView(
                    child: restaurant != true
                        ? Column(
                            children: [
                              SizedBox(
                                height: screenHeight(context, dividedBy: 15),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal:
                                        screenWidth(context, dividedBy: 20)),
                                child: Row(
                                  children: [
                                    Container(
                                      child: Text(
                                        "Available Foods",
                                        style: TextStyle(
                                          color: Constants.kitGradients[2],
                                          fontWeight: FontWeight.w400,
                                          fontSize: 23,
                                          fontFamily: "PlayFairRegular",
                                        ),
                                      ),
                                      height:
                                          screenHeight(context, dividedBy: 15),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal:
                                        screenWidth(context, dividedBy: 20)),
                                child: ListView.builder(
                                  itemCount: 5,
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemBuilder: (
                                    context,
                                    index,
                                  ) {
                                    return SearchResultTile(
                                      onPressed: () {
                                        push(context, ProductDetailPage());
                                      },
                                      price: "567",
                                      images: "assets/images/product-02.jpg",
                                      foodName: "Chicken Kebab",
                                      restarauntName: "Kabani Restaurant",
                                      rating: "3.5",
                                      onPressedAdd: () async {
                                        if (await addCart
                                                .checkSameRestaurant("3") ==
                                            false) {
                                          if (await addCart.checkItem(index) ==
                                              true) {
                                            await addCart.addItem(CartItems(
                                                results: ResultList(
                                              id: index,
                                              name: "Chicken Kebab",
                                            )));
                                          } else {
                                            addCart.foodName !=
                                                addCart.foodName;
                                            await addCart.addItem(CartItems(
                                                results: ResultList(
                                              id: index,
                                              name: "Chicken Kebab",
                                            )));
                                          }

                                          // print(addCart.cartItemBox.values
                                          //     .toString());
                                        } else {
                                          cancelAlertBoxFun(
                                              contentPadding: 80,
                                              context: context,
                                              insetPadding: 3,
                                              msg:
                                                  "Do you wish to remove the items from Kabani Restaurant?",
                                              onPressedNo: () {},
                                              onPressedYes: () async {
                                                if (await addCart
                                                        .checkItem(index) ==
                                                    true) {
                                                  await addCart
                                                      .addItem(CartItems(
                                                          results: ResultList(
                                                    id: index,
                                                    name: "Chicken Kebab",
                                                  )));
                                                } else {
                                                  addCart.foodName !=
                                                      addCart.foodName;
                                                  await addCart
                                                      .addItem(CartItems(
                                                          results: ResultList(
                                                    id: index,
                                                    name: "Chicken Kebab",
                                                  )));
                                                }
                                              },
                                              titlePadding: 0);
                                        }
                                      },
                                    );
                                  },
                                ),
                              ),
                            ],
                          )
                        : Column(
                            children: [
                              SizedBox(
                                height: screenHeight(context, dividedBy: 20),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal:
                                        screenWidth(context, dividedBy: 20)),
                                child: Row(
                                  children: [
                                    HeadingWidget(
                                      title: "Available Hotels",
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal:
                                        screenWidth(context, dividedBy: 20)),
                                child: GridView.builder(
                                  itemCount: 5,
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 2,
                                          crossAxisSpacing: 10,
                                          mainAxisSpacing: 8,
                                          childAspectRatio: 1 / 1.5),
                                  itemBuilder: (
                                    context,
                                    index,
                                  ) {
                                    return RestaurantTile(
                                      image:
                                          "https://vistapointe.net/images/restaurant-3.jpg",
                                      location: "Wayanad Restaurants",
                                    );
                                  },
                                ),
                              ),
                            ],
                          ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 10)),
              child: Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 5.6),
                  ),
                  Card(
                    child: GestureDetector(
                      child: Container(
                        width: screenWidth(context, dividedBy: 3),
                        height: screenHeight(context, dividedBy: 13),
                        color: Colors.white,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            GestureDetector(
                              onTap: () {
                                push(context, FilterPage());
                              },
                              child: Container(
                                width: screenWidth(context, dividedBy: 12),
                                child: FaIcon(
                                  FontAwesomeIcons.sortAmountUp,
                                  color: Constants.kitGradients[2],
                                  size: 30,
                                ),
                              ),
                            ),
                            Container(
                              height: screenHeight(context, dividedBy: 13),
                              width: 2,
                              color: Colors.grey,
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  restaurant = !restaurant;
                                });
                              },
                              child: Container(
                                width: screenWidth(context, dividedBy: 12),
                                child: FaIcon(
                                  restaurant == false
                                      ? FontAwesomeIcons.store
                                      : FontAwesomeIcons.hamburger,
                                  color: Constants.kitGradients[2],
                                  size: 27,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
