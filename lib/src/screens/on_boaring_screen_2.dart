import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/onboarding_2_content.dart';
import 'package:app_template/src/widgets/onboarding_content.dart';
import 'package:double_back_to_close/double_back_to_close.dart';
import 'package:flutter/material.dart';

class OnBoardingScreen2 extends StatefulWidget {
  @override
  _OnBoardingScreen2State createState() => _OnBoardingScreen2State();
}

class _OnBoardingScreen2State extends State<OnBoardingScreen2> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        // extendBodyBehindAppBar: true,
        backgroundColor: Constants.kitGradients[0],
        body: Container(
          width: screenWidth(context, dividedBy: 1),
          height: screenHeight(context, dividedBy: 1),
          child: OnBoardingContent2(
            head: "Live Tracking",
            dataHead: "Get live updates to your food ",
            data: "See live tracking of food and Delivery Boy." +
                "\nContact your Delivery Boy and get food updates.",
            image: "assets/icons/delivery_app_icon.jpg",
            buttonData: "Next",
            index: 1,
            onPressed: () {
              pushAndReplacement(context, HomePage());
            },
          ),
        ),
      ),
    );
  }
}
