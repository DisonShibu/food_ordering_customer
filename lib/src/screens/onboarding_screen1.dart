import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/on_boaring_screen_2.dart';
import 'package:app_template/src/screens/slide_transition.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/onboarding_2_content.dart';
import 'package:app_template/src/widgets/onboarding_content.dart';
import 'package:double_back_to_close/double_back_to_close.dart';
import 'package:flutter/material.dart';

class OnBoardingScreen extends StatefulWidget {
  @override
  _OnBoardingScreenState createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  int currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return DoubleBack(
      message: "Press back again to quit",
      child: SafeArea(
        child: Scaffold(
          // extendBodyBehindAppBar: true,
          backgroundColor: Constants.kitGradients[0],
          body: Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 1),
            child: currentIndex == 0
                ? OnBoardingContent(
                    head: "Home Delivery",
                    data:
                        "Order food from your home.Enjoy your time with  family and friends",
                    image: "assets/icons/delivery_app_icon.jpg",
                    buttonData: "Next",
                    index: 0,
                    onPressed: () {
                      setState(() {
                        currentIndex = 1;
                      });
                    },
                  )
                : Container(
                    width: screenWidth(context, dividedBy: 1),
                    height: screenHeight(context, dividedBy: 1),
                    child: OnBoardingContent2(
                      head: "Live Tracking",
                      dataHead: "Get live updates to your food ",
                      data: "See live tracking of food and Delivery Boy." +
                          "\nContact your Delivery Boy and get food updates.",
                      image: "assets/icons/delivery_app_icon.jpg",
                      buttonData: "Next",
                      index: 1,
                      onPressed: () {
                        pushAndReplacement(context, HomePage());
                      },
                    ),
                  ),
          ),
        ),
      ),
    );
  }
}
