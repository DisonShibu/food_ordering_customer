import 'package:app_template/src/screens/catogry_%20product_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/category_widget.dart';
import 'package:app_template/src/widgets/sub_category_widget.dart';
import 'package:flutter/material.dart';

class CategoryTilePage extends StatefulWidget {
  @override
  _CategoryTilePageState createState() => _CategoryTilePageState();
}

class _CategoryTilePageState extends State<CategoryTilePage> {
  List<String> foodImageDetails = [
    "assets/images/product-05.jpg",
    "assets/images/product-02.jpg",
    "assets/images/product-03.jpg",
    "assets/images/product-04.jpg",
    "assets/images/product-05.jpg",
    "assets/images/product-04.jpg"
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 13)),
          child: AppBarDelivery(
            title: "Sub Category",
            elevation: 3,
            onPressedLeftIcon: () {
              pop(context);
            },
            color: Constants.kitGradients[0],
            leftIcon: Icons.arrow_back_ios,
            leftIconTrue: true,
            isWhite: true,
          )),
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 40)),
        child: GridView.builder(
          padding: EdgeInsets.symmetric(
              vertical: screenHeight(context, dividedBy: 20)),
          itemCount: 5,
          physics: AlwaysScrollableScrollPhysics(),
          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 8,
            mainAxisSpacing: 8,
            childAspectRatio: 1,
          ),
          itemBuilder: (
            context,
            index,
          ) {
            return SubCategoryWidget(
              onPressed: () {
                push(context, CategoryProductPage());
              },
              images: foodImageDetails[index],
            );
          },
        ),
      ),
    );
  }
}
