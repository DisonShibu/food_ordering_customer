import 'dart:async';
import 'package:app_template/src/screens/login_page.dart';
import 'package:app_template/src/screens/onboarding_screen1.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shimmer/shimmer.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> with SingleTickerProviderStateMixin {
  Timer _timerControl;

  void startTimer() {
    _timerControl = Timer.periodic(const Duration(seconds: 3), (timer) {
      _timerControl.cancel();
      pushAndReplacement(context, LoginPage());
    });
  }

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Colors.white,
        ),
        child: Scaffold(
            backgroundColor: Constants.kitGradients[0],
            body: Builder(
                builder: (context) => SafeArea(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          child: Image.asset(
                            "assets/icons/delivery_app_icon.jpg",
                            height: screenHeight(context, dividedBy: 3),

                            // fit: BoxFit.fill,
                          ),
                        ),
                        Shimmer.fromColors(
                            child: Text(
                              "Food Delivery",
                              style: TextStyle(
                                  fontFamily: 'ProximaNova',
                                  fontSize: 30,
                                  fontWeight: FontWeight.w300,
                                  fontStyle: FontStyle.normal),
                            ),
                            baseColor: Constants.kitGradients[2],
                            highlightColor: Colors.grey),
                      ],
                    )))));
  }
}
