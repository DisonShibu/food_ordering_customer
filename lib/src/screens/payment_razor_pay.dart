// import 'package:app_template/src/utils/constants.dart';
// import 'package:app_template/src/utils/utils.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:razorpay_flutter/razorpay_flutter.dart';
//
// class PaymentRazorPay extends StatefulWidget {
//   @override
//   _PaymentRazorPayState createState() => _PaymentRazorPayState();
// }
//
// class _PaymentRazorPayState extends State<PaymentRazorPay> {
//   static const platform = const MethodChannel("razorpay_flutter");
//
//   Razorpay _razorpay;
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Constants.kitGradients[1],
//         title: const Text('Razorpay Sample App'),
//       ),
//       body: Center(
//           child: Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: <Widget>[
//             RaisedButton(onPressed: openCheckout, child: Text('Open'))
//           ])),
//     );
//   }
//
//   @override
//   void initState() {
//     super.initState();
//     _razorpay = Razorpay();
//
//     _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
//     _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
//     _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
//   }
//
//   @override
//   void dispose() {
//     super.dispose();
//     _razorpay.clear();
//   }
//
//   void openCheckout() async {
//     var options = {
//       'key': "rzp_test_QHdnjz4VtRpoQj",
//       'amount': 500 * 100,
//       'name': 'Acme Corp.',
//       "order_id": "order_I0te30vAVnkvrg",
//       "currency": "INR",
//       'description': 'Fine T-Shirt',
//       'prefill': {'contact': '8888888888', 'email': 'test@razorpay.com'},
//       'external': {
//         'wallets': ['paytm']
//       }
//     };
//
//     try {
//       _razorpay.open(options);
//     } catch (e) {
//       debugPrint(e);
//     }
//   }
//
//   void _handlePaymentSuccess(PaymentSuccessResponse response) {
//     print(response);
//     showToastSuccess("SUCCESS", context);
//   }
//
//   void _handlePaymentError(PaymentFailureResponse response) {
//     print(response.message.toString());
//     showToastError(
//         response.message
//             .toString()
//             .split(":")
//             .last
//             .replaceAll("\"", "")
//             .replaceAll("}", ""),
//         context);
//   }
//
//   void _handleExternalWallet(ExternalWalletResponse response) {
//     showToastInfo("EXTERNAL_WALLET: " + response.walletName, context);
//   }
// }
