// import 'package:app_template/src/utils/constants.dart';
// import 'package:flutter/material.dart';
// import 'package:google_maps_widget/google_maps_widget.dart';
//
// void main() {
//   runApp(GoogleMapWidget());
// }
//
// class GoogleMapWidget extends StatelessWidget {
//   LatLng source = LatLng(11.8404, 76.2030);
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: SafeArea(
//         child: Scaffold(
//           body: GoogleMapsWidget(
//             apiKey: Constants.GoogleAPIKEY,
//             sourceLatLng: LatLng(11.8404, 76.2030),
//             destinationLatLng: LatLng(11.8544, 76.2030),
//             routeWidth: 2,
//             showPolyline: true,
//             routeColor: Colors.red,
//             showSourceMarker: true,
//             defaultCameraLocation: source,
//             sourceMarkerIconInfo: MarkerIconInfo(
//               assetPath: "assets/images/house-marker-icon.png",
//             ),
//             destinationMarkerIconInfo: MarkerIconInfo(
//               assetPath: "assets/images/restaurant-marker-icon.png",
//             ),
//             driverMarkerIconInfo: MarkerIconInfo(
//               assetPath: "assets/images/driver-marker-icon.png",
//               assetMarkerSize: Size.square(125),
//             ),
//             // mock stream
//             showDestinationMarker: false,
//             driverCoordinatesStream:
//                 Stream.periodic(Duration(milliseconds: 500), (i) {
//               source = LatLng(
//                 11.8544 + i / 10000,
//                 76.2030 - i / 10000,
//               );
//               return LatLng(
//                 11.8544 + i / 10000,
//                 76.2030 - i / 10000,
//               );
//
//               //   ,
//               // }
//             }),
//             sourceName: "This is source name",
//             driverName: "Alex",
//             onTapDriverMarker: (currentLocation) {
//               print("Driver is currently at $currentLocation");
//             },
//             totalTimeCallback: (time) => print(time + "Time Remaining"),
//             totalDistanceCallback: (distance) => print(distance),
//           ),
//         ),
//       ),
//     );
//   }
// }
