import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HelpAndSupport extends StatefulWidget {
  @override
  _HelpAndSupportState createState() => _HelpAndSupportState();
}

class _HelpAndSupportState extends State<HelpAndSupport> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
            preferredSize:
                Size.fromHeight(screenHeight(context, dividedBy: 13)),
            child: AppBarDelivery(
              title: "Help and Support",
              elevation: 0,
              onPressedLeftIcon: () {
                pop(context);
              },
              color: Constants.kitGradients[0],
              leftIcon: Icons.arrow_back_ios,
              leftIconTrue: true,
              isWhite: true,
            )),
        body: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 20)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 40),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 1.2),
                  child: Text(
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
                    "Why do we use it?"
                    " It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like.",
                    style: TextStyle(
                        fontSize: screenWidth(context, dividedBy: 18),
                        color: Colors.black,
                        fontWeight: FontWeight.w400,
                        fontFamily: "SofiaProRegular"),
                    overflow: TextOverflow.visible,
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
