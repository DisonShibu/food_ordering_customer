import 'dart:io';

import 'package:app_template/src/screens/otp_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/build_icon_button.dart';
import 'package:app_template/src/widgets/login_text_box.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ApplyCoupon extends StatefulWidget {
  @override
  _ApplyCouponState createState() => _ApplyCouponState();
}

class _ApplyCouponState extends State<ApplyCoupon> {
  TextEditingController couponTextEditingController;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: ListView(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 10)),
            child: Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 10),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 2.6),
                  height: screenHeight(context, dividedBy: 5),
                  child: Image.asset(
                    "assets/icons/delivery_app_icon.jpg",
                    fit: BoxFit.fill,
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),
                Row(
                  children: [
                    Container(
                        width: screenWidth(context, dividedBy: 1.7),
                        child: FittedBox(
                            child: Text(
                          "Apply Coupon!",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 30,
                              fontFamily: 'PromptLight'),
                        ))),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.white,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      LoginTextBox(
                        phoneNumber: false,
                        controller: couponTextEditingController,
                        icon: Icons.card_giftcard,
                        hintText: "Enter Your Code",
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),
                BuildButton(
                  onPressed: () {
                    if (couponTextEditingController.text != null) {}
                  },
                  title: "APPLY CODE",
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
