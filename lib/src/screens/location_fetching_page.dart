import 'dart:async';
import 'dart:io';

import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/state/app_state.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/address_heading_widget.dart';
import 'package:app_template/src/widgets/loading.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

class LocationFetchingPage extends StatefulWidget {
  @override
  _LocationFetchingPageState createState() => _LocationFetchingPageState();
}

class _LocationFetchingPageState extends State<LocationFetchingPage> {
  bool loading = true;
  Position currentLocation;
  LatLng initialPosition;
  String address;
  bool appSettings = false;

  Future fetchLocation() async {
    await _getUserAddress();
  }

  @override
  void initState() {
    fetchLocation();

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool locatioState = loading;
    print("Loading" + loading.toString());
    return Scaffold(
        body: locatioState == false
            ? Center(
              child: Column(
                  children: [
                    AddressHeadingWidget(
                      title: "Location Fetched",
                    ),
                  ],
                ),
            )
            : Loading(
                message: "Location fetching....",
              ));
  }

  /// Determine the current position of the device.
  ///
  /// When the location services are not enabled or permissions
  /// are denied the `Future` will return an error.
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {}
    if (serviceEnabled != true) {
      showToast("Please Enable Your Location Service");

      Future.error("Location Service Disabled");
    }

    permission = await Geolocator.checkPermission();

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();

      setState(() {
        loading = false;
      });

      push(context, HomePage());
    }

    if (permission == LocationPermission.deniedForever) {
      setState(() {
        loading = false;
      });

      if (Platform.isAndroid) {
        await Geolocator.openLocationSettings();
      } else {
        push(context, HomePage());
      }
      return Future.error("Location Service Are Denied");
      // Permissions are denied forever, handle appropriately.

    }

    return await Geolocator.getCurrentPosition();
  }

  void _getUserAddress() async {
    print("GET USER METHOD RUNNING =========");
    _determinePosition().then((position) async {
      print("Location Fetched");
      setState(() {
        loading = false;
      });
      currentLocation = position;
      List<Placemark> placemark = await placemarkFromCoordinates(
          currentLocation.latitude, currentLocation.longitude);
      initialPosition =
          LatLng(currentLocation.latitude, currentLocation.longitude);
      print(
          "the latitude is: ${currentLocation.longitude} and th longitude is: ${currentLocation.longitude} ");
      print("initial position is : ${initialPosition.toString()}");
      address = placemark[0].street +
          placemark[0].subLocality +
          placemark[0].locality;
      await ObjectFactory().appHive.putLanguage(address);
      ObjectFactory()
          .appHive
          .putUserLatitude(currentLocation.latitude.toString());
      ObjectFactory()
          .appHive
          .getUserLongitude(currentLocation.longitude.toString());
      Provider.of<HomeState>(context, listen: false)
          .locationStatus(value: true);
      Provider.of<HomeState>(context, listen: false).locationValueStored(
          locationCoordinates: currentLocation, value: address);

      push(context, HomePage());
    }).onError((error, stackTrace) {
      push(context, HomePage());
    });
  }
}
