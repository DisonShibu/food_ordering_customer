import 'dart:io';

import 'package:app_template/src/screens/cart_page.dart';
import 'package:app_template/src/screens/category_tile_page.dart';
import 'package:app_template/src/screens/drawer.dart';
import 'package:app_template/src/screens/my_orders_page.dart';
import 'package:app_template/src/screens/notification_page.dart';
import 'package:app_template/src/screens/search_page.dart';
import 'package:app_template/src/state/app_state.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar_home_page.dart';
import 'package:app_template/src/widgets/category_widget.dart';
import 'package:app_template/src/widgets/home_page_offer.dart';
import 'package:app_template/src/widgets/home_tab_bar.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:double_back_to_close/double_back_to_close.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  bool isAppStartUp;
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController searchTextEditingController =
      new TextEditingController();
  final GlobalKey<ScaffoldState> _globalKey = new GlobalKey<ScaffoldState>();
  List<String> imageList = [
    "assets/images/slide-bg-1.jpg",
    "assets/images/slide-bg-2.jpg",
    "assets/images/slide-bg-3.jpg",
    "assets/images/slide-bg-4.jpg",
  ];
  List<String> foodImageDetails = [
    "assets/images/product-05.jpg",
    "assets/images/product-02.jpg",
    "assets/images/product-03.jpg",
    "assets/images/product-04.jpg",
    "assets/images/product-05.jpg",
    "assets/images/product-04.jpg"
  ];
  Future<bool> willPopScope() async {
    exit(0);
    return Future<bool>.value(true);
  }

  int currentIndex = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: willPopScope,
      child: DoubleBack(
        message: "Press again to quit",
        child: SafeArea(
          child: Scaffold(
              drawer: DrawerWidget(),
              key: _globalKey,
              resizeToAvoidBottomInset: false,
              backgroundColor: Constants.kitGradients[0],
              floatingActionButton: HomeTabBar(
                  currentIndex: 0,
                  onTapHome: () {},
                  onTapSearch: () {
                    push(context, SearchPage());
                  },
                  onTapOrder: () {
                    push(context, MyOrdersPage());
                  },
                  onTapCart: () {
                    push(context, CartPage());
                  }),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerDocked,
              body: Column(
                children: [
                  AppBarHomePage(
                      searchTextEditingController: searchTextEditingController,
                      onPressedLeft: () {
                        _globalKey.currentState.openDrawer();
                      },
                      onPressedRight: () {
                        push(context, NotificationPage());
                      },
                      location: Provider.of<HomeState>(context, listen: false)
                                  .isAppStartUp ==
                              AppStartUp.NO
                          ? Provider.of<HomeState>(context, listen: false)
                                      .address !=
                                  null
                              ? Provider.of<HomeState>(context, listen: false)
                                  .address
                              : "Your Location Here"
                          : Provider.of<HomeState>(context, listen: true)
                              .address),
                  Container(
                    height: screenHeight(context, dividedBy: 1.43),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      physics: AlwaysScrollableScrollPhysics(),
                      child: Column(children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 50),
                        ),
                        CarouselSlider(
                          options: CarouselOptions(
                              aspectRatio: 2,
                              onPageChanged: (index, reason) {
                                currentIndex = index;
                                print(currentIndex);
                                setState(() {});
                              },
                              reverse: false,
                              initialPage: 0,
                              viewportFraction: 1,
                              autoPlay: false),
                          items: imageList.map((i) {
                            return Builder(builder: (BuildContext context) {
                              return HomePageOffer(
                                imgListLength: imageList.length,
                                onPressed: () {},
                                images: i,
                                currentIndex: currentIndex,
                              );
                            });
                          }).toList(),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: screenWidth(context, dividedBy: 40)),
                          child: Column(
                            children: [
                              SizedBox(
                                height: screenHeight(context, dividedBy: 50),
                              ),
                              Row(
                                children: [
                                  Text(
                                    "Categories",
                                    style: TextStyle(
                                      color: Constants.kitGradients[2],
                                      fontWeight: FontWeight.w400,
                                      fontSize: 23,
                                      fontFamily: "OswaldRegular",
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: screenWidth(context, dividedBy: 40)),
                          child: GridView.builder(
                            itemCount: 5,
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              crossAxisSpacing: 8,
                              mainAxisSpacing: 8,
                              childAspectRatio: 1,
                            ),
                            itemBuilder: (
                              context,
                              index,
                            ) {
                              return CategoryWidget(
                                onPressed: () {
                                  push(context, CategoryTilePage());
                                },
                                images: foodImageDetails[index],
                              );
                            },
                          ),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 10),
                        ),
                      ]),
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }
}
