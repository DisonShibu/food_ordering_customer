import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/onboarding_screen1.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/restuarant_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:animated_otp_fields/animated_otp_fields.dart';

class OtpPage extends StatefulWidget {
  @override
  _OtpPageState createState() => _OtpPageState();
}

class _OtpPageState extends State<OtpPage> {
  TextEditingController otpTextEditingController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        height: screenHeight(context, dividedBy: 1),
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 20)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 10),
              ),
              Center(
                  child: Text(
                "Verification Code",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 30,
                    fontFamily: 'OswaldRegular'),
              )),
              SizedBox(
                height: screenHeight(context, dividedBy: 20),
              ),
              Center(
                  child: Text(
                "Please enter the verification code sent to 759879799. ",
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 20,
                    fontFamily: 'OswaldRegular'),
                textAlign: TextAlign.center,
              )),
              SizedBox(
                height: screenHeight(context, dividedBy: 10),
              ),
              animated_otp_fields(
                otpTextEditingController,
                fieldHeight: 50,
                fieldWidth: MediaQuery.of(context).size.width / 9,
                OTP_digitsCount: 6,
                animation: TextAnimation.Fading,
                spaceBetweenFields: 10,
                border: Border.all(width: 3, color: Constants.kitGradients[2]),
                borderRadius: BorderRadius.all(Radius.circular(10)),
                contentPadding: EdgeInsets.only(bottom: 3),
                forwardCurve: Curves.linearToEaseOut,
                textStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 30,
                    fontWeight: FontWeight.bold),
                onFieldSubmitted: (text) {},
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 13),
              ),
              Center(
                  child: Text(
                "Do not receive the OTP? ",
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 20,
                    fontFamily: 'OswaldRegular'),
                textAlign: TextAlign.center,
              )),
              SizedBox(
                height: screenHeight(context, dividedBy: 50),
              ),
              Center(
                  child: Text(
                "Resend OTP? ",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontFamily: 'OswaldRegular',
                    decoration: TextDecoration.underline,
                    decorationColor: Colors.black),
                textAlign: TextAlign.center,
              )),
              SizedBox(
                height: screenHeight(context, dividedBy: 8),
              ),
              RestaurantButton(
                title: "Verify",
                onPressed: () {
                  pushAndReplacement(context, OnBoardingScreen());
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
