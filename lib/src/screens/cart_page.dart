import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/saved_location_page.dart';
import 'package:app_template/src/screens/my_orders_page.dart';
import 'package:app_template/src/screens/place_order_page.dart';
import 'package:app_template/src/screens/search_page.dart';
import 'package:app_template/src/state/app_state.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/build_icon_button.dart';
import 'package:app_template/src/widgets/driver_name_widget.dart';
import 'package:app_template/src/widgets/home_tab_bar.dart';
import 'package:app_template/src/widgets/list_tile_myorders.dart';
import 'package:app_template/src/widgets/list_tile_cart.dart';
import 'package:app_template/src/widgets/restuarant_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  TextEditingController notesTextEditingController =
      new TextEditingController();
  bool submitted;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Constants.kitGradients[0],
      floatingActionButton: HomeTabBar(
          currentIndex: 3,
          onTapHome: () {
            push(context, HomePage());
          },
          onTapSearch: () {
            push(context, SearchPage());
          },
          onTapOrder: () {
            push(context, MyOrdersPage());
          },
          onTapCart: () {}),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 13)),
          child: AppBarDelivery(
            title: "Cart Page",
            elevation: 0,
            onPressedLeftIcon: () {
              pop(context);
            },
            color: Constants.kitGradients[0],
            leftIcon: Icons.arrow_back_ios,
            leftIconTrue: true,
            isWhite: true,
          )),
      body: Padding(
        padding: EdgeInsets.only(top: screenWidth(context, dividedBy: 20)),
        child: Column(
          children: [
            Container(
              height: screenHeight(context, dividedBy: 1.2),
              child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  physics: AlwaysScrollableScrollPhysics(),
                  addAutomaticKeepAlives: true,
                  padding: EdgeInsets.zero,
                  itemCount: 2,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children: [
                        ListTileCart(
                          foodImage:
                              "https://images.saymedia-content.com/.image/t_share/MTc0Mzc4NTY2MjIwNjUzOTI4/best-burger-restaurant-names.jpg",
                          name: "Burger",
                          address: "Kakkanattu (H),Seethamount (P.0),Pulpally",
                          orderNum: "ORD567",
                          restaurantName: "PizzaWorld",
                          onPressedClear: () {
                            var addCart =
                                Provider.of<HomeState>(context, listen: false);
                            addCart.clearHiveItem("2");
                          },
                          onValueChanged: () {},
                        ),
                        index == 1
                            ? Column(
                                children: [
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 80),
                                  ),
                                  Divider(
                                    color: Colors.grey,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal:
                                            screenWidth(context, dividedBy: 5)),
                                    child: DriverNameWidget(
                                      containerWidth: 2.6,
                                      profileImageTrue: false,
                                      leftIconTrue: true,
                                      onPressedLeftIcon: () {},
                                      iconLeft: Icons.edit,
                                      onPressedRightIcon: () {},
                                      onPressedText: () {
                                        submitted = false;
                                        reviewDialog(
                                            contentPadding: 80,
                                            context: context,
                                            insetPadding: 3,
                                            msg: "Notes",
                                            reviewTexteditingController:
                                                notesTextEditingController,
                                            onPressed: () {
                                              submitted = true;
                                              setState(() {});
                                            },
                                            titlePadding: 0);
                                      },
                                      title: notesTextEditingController.text ==
                                                  "" ||
                                              submitted != true
                                          ? "Add Custom Notes"
                                          : notesTextEditingController.text,
                                      richTextSubHeading: "",
                                      driverMessageTrue: false,
                                      richTextTrue: true,
                                      rightIconTrue: false,
                                    ),
                                  ),
                                  Divider(
                                    color: Colors.grey,
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 90),
                                  ),
                                  Container(
                                    width: screenWidth(context, dividedBy: 2),
                                    child: BuildIconButton(
                                      onPressed: () {
                                        push(context, SavedLocationPage());
                                      },
                                      title: "CHANGE ",
                                      icon: "assets/images/location.svg",
                                    ),
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 40),
                                  ),
                                  RestaurantButton(
                                    title: "CONTINUE",
                                    onPressed: () {
                                      push(context, PlaceOrderPage());
                                    },
                                  ),
                                  SizedBox(
                                    height: screenHeight(context, dividedBy: 9),
                                  ),
                                ],
                              )
                            : Container()
                      ],
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
