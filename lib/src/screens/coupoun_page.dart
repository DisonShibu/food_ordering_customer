import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/list_tile_myorders.dart';
import 'package:flutter/material.dart';

class CouponsPage extends StatefulWidget {
  @override
  _CouponsPageState createState() => _CouponsPageState();
}

class _CouponsPageState extends State<CouponsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[0],
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 13)),
          child: AppBarDelivery(
            title: "Coupons",
            elevation: 3,
            onPressedLeftIcon: () {
              pop(context);
            },
            color: Constants.kitGradients[0],
            leftIcon: Icons.arrow_back_ios,
            leftIconTrue: true,
            isWhite: true,
          )),
      body: Padding(
        padding: EdgeInsets.only(top: screenWidth(context, dividedBy: 20)),
        child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            physics: AlwaysScrollableScrollPhysics(),
            addAutomaticKeepAlives: true,
            padding: EdgeInsets.zero,
            itemCount: 2,
            itemBuilder: (BuildContext context, int index) {
              return ListTileMyOrders(
                orderNumber: false,
                isPrice: false,
                textCenter: true,
                doubleWidth: 5,
                doubleHeight: 10,
                foodImage:
                    "https://www.pngitem.com/pimgs/m/215-2151327_discount-offers-and-deals-hd-png-download.png",
                name: "CODE56789",
                address: "Summer Discounts ",
                orderNum: "",
                price: "50" "%",
              );
            }),
      ),
    );
  }
}
