import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/add_button.dart';
import 'package:app_template/src/widgets/address_heading_widget.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/circular_container_icon.dart';
import 'package:app_template/src/widgets/delivery_icon_widget.dart';
import 'package:app_template/src/widgets/heading_widget.dart';
import 'package:app_template/src/widgets/rating_widget.dart';
import 'package:app_template/src/widgets/review_button.dart';
import 'package:flutter/material.dart';

class ReviewPage extends StatefulWidget {
  @override
  _ReviewPageState createState() => _ReviewPageState();
}

class _ReviewPageState extends State<ReviewPage> {
  bool isMoreReview = false;
  bool isMoreReviewAvailable = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 13)),
          child: AppBarDelivery(
            title: "Food Reviews",
            elevation: 0,
            onPressedLeftIcon: () {
              pop(context);
            },
            color: Constants.kitGradients[0],
            leftIcon: Icons.arrow_back_ios,
            leftIconTrue: true,
            isWhite: true,
          )),
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 18)),
        child: ListView.builder(
            itemCount: isMoreReview == false ? 3 : 5,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: [
                  index == 0
                      ? Column(
                          children: [
                            Row(
                              children: [
                                HeadingWidget(
                                  title: "Reviews",
                                ),
                              ],
                            ),
                            Container(
                              width: screenWidth(context, dividedBy: 1),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  CircularContainerWidget(
                                    icon: Icons.star_border,
                                    boxColor: Constants.kitGradients[2]
                                        .withOpacity(0.6),
                                    iconColor: Constants.kitGradients[2],
                                    radius: 18,
                                  ),
                                  SizedBox(
                                    width: screenWidth(context, dividedBy: 200),
                                  ),
                                  HeadingWidget(
                                    title: "3.6",
                                  ),
                                  Spacer(
                                    flex: 1,
                                  ),
                                  ReviewButton(
                                    title: "Add",
                                    onPressed: () {},
                                  )
                                ],
                              ),
                            ),
                          ],
                        )
                      : Container(),
                  ReviewWidget(
                    reviewTitle: "Awsome Food",
                    body:
                        "Nice Spicy and Fresh, it is variety food.Rare to get the food. My Family fell in love with the foo",
                  ),
                  isMoreReviewAvailable == true
                      ? isMoreReview == false
                          ? index == 2
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        isMoreReview = true;
                                        setState(() {});
                                      },
                                      child: Text(
                                        "MoreReview",
                                        style: TextStyle(
                                          color: Constants.kitGradients[2],
                                          fontWeight: FontWeight.w400,
                                          fontSize: 23,
                                          fontFamily: "PlayFairRegular",
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              : Container()
                          : index == 4
                              ? Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        GestureDetector(
                                          onTap: () {
                                            isMoreReview = false;
                                            setState(() {});
                                          },
                                          child: Text(
                                            "Less Review",
                                            style: TextStyle(
                                              color: Constants.kitGradients[2],
                                              fontWeight: FontWeight.w400,
                                              fontSize: 23,
                                              fontFamily: "PlayFairRegular",
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 40),
                                    ),
                                  ],
                                )
                              : Container()
                      : Container(),
                ],
              );
            }),
      ),
    );
  }
}
