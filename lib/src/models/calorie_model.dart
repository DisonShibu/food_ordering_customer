import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class CalorieModel {
  String items;
  int percentage;
  charts.Color barColor;
  CalorieModel({this.barColor, this.items, this.percentage});
}
