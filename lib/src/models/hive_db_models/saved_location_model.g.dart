// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'saved_location_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SavedLocations extends TypeAdapter<SavedLocationsModel> {
  @override
  final int typeId = 0;

  @override
  SavedLocationsModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SavedLocationsModel(
      location: fields[1] as String,
      taggedPlace: fields[0] as String,
      latLng: fields[2] as LatLng,
    );
  }

  @override
  void write(BinaryWriter writer, SavedLocationsModel obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.taggedPlace)
      ..writeByte(1)
      ..write(obj.location)
      ..writeByte(2)
      ..write(obj.latLng);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SavedLocations &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
