import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hive/hive.dart';
part 'saved_location_model.g.dart';

@HiveType(typeId: 0, adapterName: "savedlocations")
class SavedLocationsModel {
  @HiveField(0)
  final String taggedPlace;
  @HiveField(1)
  final String location;
  @HiveField(2)
  final LatLng latLng;

  SavedLocationsModel({this.location, this.taggedPlace, this.latLng});
}
