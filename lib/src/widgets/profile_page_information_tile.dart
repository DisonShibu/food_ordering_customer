import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProfilePageInformationTile extends StatefulWidget {
  final String icon;
  final String title;
  final Function onPressed;
  final bool isArrow;
  final bool promoCodeTrue;
  final double doublePadding;
  final bool checkBoxTrue;
  final ValueChanged onChecked;
  ProfilePageInformationTile(
      {this.icon,
      this.title,
      this.onPressed,
      this.isArrow,
      this.promoCodeTrue,
      this.onChecked,
      this.checkBoxTrue,
      this.doublePadding});

  @override
  _ProfilePageInformationTileState createState() =>
      _ProfilePageInformationTileState();
}

class _ProfilePageInformationTileState
    extends State<ProfilePageInformationTile> {
  bool checked = false;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Container(
        height: screenHeight(context, dividedBy: 18),
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: widget.promoCodeTrue == true
                  ? widget.doublePadding
                  : screenWidth(context, dividedBy: 30)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                  width: screenWidth(context, dividedBy: 14),
                  height: screenHeight(context, dividedBy: 20),
                  decoration: BoxDecoration(
                      color: Constants.kitGradients[1].withOpacity(0.3),
                      shape: BoxShape.circle),
                  child: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Image.asset(
                      widget.icon,
                    ),
                  )),
              Spacer(
                flex: 1,
              ),
              Container(
                width: screenWidth(context, dividedBy: 3),
                child: Text(
                  widget.title,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontFamily: 'OswaldRegular'),
                ),
              ),
              widget.isArrow == false
                  ? Spacer(
                      flex: 3,
                    )
                  : Spacer(
                      flex: 3,
                    ),
              widget.isArrow == false
                  ? widget.checkBoxTrue == true
                      ? Checkbox(
                          value: checked,
                          activeColor: Constants.kitGradients[2],
                          onChanged: (value) {
                            widget.onChecked(value);
                            setState(() {
                              checked = value;
                            });
                          },
                        )
                      : Container()
                  : Icon(
                      Icons.arrow_forward_ios_outlined,
                      size: 17,
                      color: Colors.black,
                    )
            ],
          ),
        ),
      ),
    );
  }
}
