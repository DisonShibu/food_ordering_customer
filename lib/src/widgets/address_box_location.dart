import 'package:app_template/src/widgets/address_box_text_feild.dart';
import 'package:app_template/src/widgets/address_heading_widget.dart';
import 'package:app_template/src/widgets/description_textfield.dart';
import 'package:flutter/material.dart';

import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class AddressLocationBox extends StatefulWidget {
  final Function onPressedAddress;
  final String hintText;
  final String subHeading;
  final IconData icon;
  final ValueChanged onTextChanged;
  final TextEditingController textEditingController;
  AddressLocationBox(
      {this.onPressedAddress,
      this.hintText,
      this.onTextChanged,
      this.subHeading,
      this.textEditingController,
      this.icon});
  @override
  _AddressLocationBoxState createState() => _AddressLocationBoxState();
}

class _AddressLocationBoxState extends State<AddressLocationBox> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressedAddress();
      },
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: screenWidth(context, dividedBy: 20),
              ),
              Container(
                  width: screenWidth(context, dividedBy: 1.2),
                  height: screenHeight(context, dividedBy: 15),
                  child: AddressBoxTextFeild(
                    searchTextEditingController: widget.textEditingController,
                    title: widget.hintText,
                    icon: widget.icon,
                  ))
            ],
          ),
          // Divider(
          //   color: Colors.black26,
          //   thickness: 1.0,
          // )
        ],
      ),
    );
  }
}
