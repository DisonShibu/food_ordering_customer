import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProfileIconWidget extends StatefulWidget {
  final String phoneNumber;
  final String imageName;
  final String userName;
  final double fontSizeHeading;
  final double fontSizedPhoneNUmber;
  final double imagePadding;
  final Color color;
  ProfileIconWidget(
      {this.color,
      this.imageName,
      this.phoneNumber,
      this.userName,
      this.fontSizedPhoneNUmber,
      this.imagePadding,
      this.fontSizeHeading});
  @override
  _ProfileIconWidgetState createState() => _ProfileIconWidgetState();
}

class _ProfileIconWidgetState extends State<ProfileIconWidget> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        CircularProfileAvatar(
          null,
          child: Image.asset(
            "assets/images/userprofile.png",
            color: Colors.black,
          ),
          backgroundColor: Colors.white,
          borderWidth: 2,
          elevation: 0,
          radius: 50,
        ),
        SizedBox(
          width: screenWidth(context, dividedBy: 40),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              widget.userName,
              style: TextStyle(
                  fontSize: widget.fontSizeHeading,
                  color: widget.color,
                  fontFamily: 'PrompLight'),
            ),
            SizedBox(
              height: 3,
            ),
            Text(
              widget.phoneNumber,
              style: TextStyle(
                  fontSize: widget.fontSizedPhoneNUmber,
                  color: widget.color,
                  fontFamily: 'PrompLight'),
            ),
            SizedBox(
              width: screenWidth(context, dividedBy: 40),
            ),
          ],
        ),
      ],
    );
  }
}
