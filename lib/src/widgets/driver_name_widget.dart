import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/chat_bubble_clipper.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_audio_recorder2/flutter_audio_recorder2.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DriverNameWidget extends StatefulWidget {
  final IconData iconRight;
  final String title;
  final Function onPressed;
  final IconData iconLeft;
  final bool rightIconTrue;
  final bool driverMessageTrue;
  final String driverMessage;
  final Function onPressedLeftIcon;
  final Function onPressedRightIcon;
  final double containerWidth;
  final bool profileImageTrue;
  final bool chatBox;
  final bool leftIconTrue;
  final Function onPressedText;
  final bool richTextTrue;
  final String richTextSubHeading;
  final bool reducePadding;
  final String profilePic;
  DriverNameWidget(
      {this.iconLeft,
      this.iconRight,
      this.title,
      this.onPressed,
      this.driverMessageTrue,
      this.driverMessage,
      this.onPressedLeftIcon,
      this.containerWidth,
      this.chatBox,
      this.profileImageTrue,
      this.rightIconTrue,
      this.onPressedText,
      this.onPressedRightIcon,
      this.richTextSubHeading,
      this.richTextTrue,
      this.reducePadding,
      this.profilePic,
      this.leftIconTrue});
  @override
  _DriverNameWidgetState createState() => _DriverNameWidgetState();
}

class _DriverNameWidgetState extends State<DriverNameWidget> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        print("kjhkjhkjhjkhkjh");
      },
      child: Container(
        width: screenWidth(context, dividedBy: 1),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                GestureDetector(
                    onTap: () {
                      widget.onPressedLeftIcon();
                    },
                    child: widget.profileImageTrue == true
                        ? CircularProfileAvatar(
                            "",
                            child: Image.asset(
                              widget.profilePic,
                              fit: BoxFit.fill,
                            ),
                            backgroundColor: Constants.kitGradients[0],
                            borderWidth: 2,
                            borderColor: Constants.kitGradients[2],
                            elevation: 0,
                            radius: 20,
                          )
                        : widget.leftIconTrue == true
                            ? Container(
                                width: screenWidth(context, dividedBy: 10),
                                height: screenHeight(context, dividedBy: 20),
                                decoration: BoxDecoration(
                                    color: Constants.kitGradients[1]
                                        .withOpacity(0.3),
                                    shape: BoxShape.circle),
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Icon(
                                    widget.iconLeft,
                                    size: 24,
                                    color: Constants.kitGradients[12],
                                  ),
                                ))
                            : Container()),
                widget.leftIconTrue == true || widget.profileImageTrue == true
                    ? Spacer(
                        flex: 1,
                      )
                    : Container(),
                GestureDetector(
                  onTap: () {
                    widget.onPressedText();
                  },
                  child: Container(
                      width: screenWidth(context,
                          dividedBy: widget.containerWidth),
                      child: widget.richTextTrue != true
                          ? RichText(
                              text: TextSpan(
                                text: widget.title,
                                style: TextStyle(
                                  color: Constants.kitGradients[5],
                                  fontSize: screenWidth(context, dividedBy: 23),
                                  fontFamily: "OswaldRegular",
                                ),
                                children: <TextSpan>[
                                  TextSpan(
                                      text: widget.richTextSubHeading,
                                      style: TextStyle(
                                          color: Colors.grey,
                                          fontSize: screenWidth(context,
                                              dividedBy: 26),
                                          fontFamily: "OpenSansRegular",
                                          decorationColor: Colors.grey)),
                                ],
                              ),
                            )
                          : RichText(
                              text: TextSpan(
                                text: widget.title,
                                style: TextStyle(
                                  color: Constants.kitGradients[5],
                                  fontSize: screenWidth(context, dividedBy: 29),
                                  fontFamily: "OpenSansRegular",
                                ),
                                children: <TextSpan>[
                                  TextSpan(
                                      text: widget.richTextSubHeading,
                                      style: TextStyle(
                                          color: Constants.kitGradients[12],
                                          fontSize: screenWidth(context,
                                              dividedBy: 29),
                                          fontFamily: "OpenSansRegular",
                                          decoration: TextDecoration.underline,
                                          decorationColor:
                                              Constants.kitGradients[12])),
                                ],
                              ),
                            )),
                ),
                widget.rightIconTrue == false
                    ? Spacer(
                        flex: 1,
                      )
                    : Spacer(
                        flex: 3,
                      ),
                GestureDetector(
                  onTap: () {
                    widget.onPressedRightIcon();
                  },
                  child: widget.rightIconTrue == false
                      ? Container()
                      : Container(
                          width: screenWidth(context, dividedBy: 14),
                          height: screenHeight(context, dividedBy: 20),
                          decoration: BoxDecoration(
                              color: Constants.kitGradients[1].withOpacity(0.3),
                              shape: BoxShape.circle),
                          child: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Icon(
                                widget.iconRight,
                                size: 20,
                                color: Colors.red,
                              ))),
                ),
              ],
            ),
            widget.chatBox == true
                ? SizedBox(
                    height: screenHeight(context, dividedBy: 100),
                  )
                : Container(),
            widget.chatBox == true
                ? ClipPath(
                    clipper: ChatBubbleClipper1(),
                    child: Container(
                      height: screenHeight(context, dividedBy: 20),
                      width: screenWidth(context, dividedBy: 1.2),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: Constants.kitGradients[1].withOpacity(0.4)),
                      child: Center(
                        child: Text(
                          widget.driverMessage,
                          style: TextStyle(
                              fontFamily: "OpenSansRegular",
                              fontSize: 12,
                              fontWeight: FontWeight.w800,
                              color: Constants.kitGradients[2]),
                        ),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
