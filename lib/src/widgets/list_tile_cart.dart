import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/add_athelete_number_widget.dart';
import 'package:app_template/src/widgets/cart_page_image_tile.dart';
import 'package:app_template/src/widgets/restuarant_button.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

// ignore: must_be_immutable
class ListTileCart extends StatefulWidget {
  final String foodImage;
  final String name;
  final String orderNum;
  final String address;
  final String restaurantName;
  final onValueChanged;
  final Function onPressedClear;
  ListTileCart({
    this.foodImage,
    this.restaurantName,
    this.name,
    this.orderNum,
    this.onValueChanged,
    this.address,
    this.onPressedClear,
  });
  @override
  _ListTileCartState createState() => _ListTileCartState();
}

class _ListTileCartState extends State<ListTileCart> {
  int count = 1;
  int price = 200;
  void incrementFunction() {
    if (count <= 10) {
      setState(() {
        count = count + 1;
      });
    } else {
      setState(() {});
    }
  }

  void decrementFunction() {
    if (count > 1) {
      setState(() {
        count = count - 1;
      });
    } else {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 40)),
      child: Card(
        color: Constants.kitGradients[0],
        elevation: 3,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 40)),
          child: Column(
            children: [
              CartPageImageTile(
                  image: widget.foodImage,
                  restaurantName: widget.restaurantName,
                  foodName: widget.name,
                  containerHeight: 8,
                  containerWidth: 2.4,
                  onPressedClear: widget.onPressedClear),
              SizedBox(
                height: screenHeight(context, dividedBy: 80),
              ),
              Container(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    // Row(
                    //   crossAxisAlignment: CrossAxisAlignment.start,
                    //   children: [
                    //     Icon(
                    //       Icons.radio_button_checked,
                    //       size: 20,
                    //       color: Colors.red,
                    //     ),
                    //     SizedBox(
                    //       width: screenWidth(context, dividedBy: 50),
                    //     ),
                    //     Container(
                    //       width: screenWidth(context, dividedBy: 3.2),
                    //       child: Text(
                    //         "burger , chesse , almond,chicken",
                    //         style: TextStyle(
                    //             color: Constants.kitGradients[10],
                    //             fontSize: screenWidth(context, dividedBy: 27),
                    //             fontFamily: "OpenSansRegular"),
                    //       ),
                    //     ),
                    //   ],
                    // ),
                    Container(
                      height: screenHeight(context, dividedBy: 19),
                      child: AddAthleteNumber(
                        count: count,
                        decrementOnTap: decrementFunction,
                        incrementOnTap: incrementFunction,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: screenWidth(context, dividedBy: 80)),
                      child: Container(
                        width: screenWidth(context, dividedBy: 7),
                        child: Text(
                          "Price\n " + (200 * count).toString() + " SR",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Constants.kitGradients[3],
                              fontFamily: "PrompLight",
                              fontSize: screenWidth(context, dividedBy: 29)),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 50),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    "Item Total: 567" + " SR",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Constants.kitGradients[3],
                        fontFamily: "PromPLight",
                        fontSize: screenWidth(context, dividedBy: 29)),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 50),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
