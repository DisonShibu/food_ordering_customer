import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class LocationTextBox extends StatefulWidget {
  final ValueChanged onLocationChanged;
  final Function onPressed;
  final String heading;
  final IconData icon;
  final double padding;
  final bool dividerTrue;
  final bool subHeadingTrue;
  final double boxWidth;
  final bool colorWhite;
  LocationTextBox(
      {this.onLocationChanged,
      this.heading,
      this.icon,
      this.onPressed,
      this.dividerTrue,
      this.subHeadingTrue,
      this.boxWidth,
      this.colorWhite,
      this.padding});
  @override
  _LocationTextBoxState createState() => _LocationTextBoxState();
}

class _LocationTextBoxState extends State<LocationTextBox> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: widget.padding),
        ),
        child: Column(
          children: [
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Icon(
                      widget.icon,
                      size: screenWidth(context, dividedBy: 14),
                      color: widget.colorWhite == true
                          ? Colors.white
                          : Colors.black26,
                    ),
                  ),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 20),
                  ),
                  Column(
                    children: [
                      Container(
                        width: screenWidth(context, dividedBy: widget.boxWidth),
                        child: Text(
                          widget.heading,
                          style: TextStyle(
                              color: widget.colorWhite == true
                                  ? Constants.kitGradients[0]
                                  : Constants.kitGradients[5],
                              fontFamily: "PrompLight",
                              fontSize: screenWidth(context, dividedBy: 26)),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            widget.dividerTrue == true
                ? Divider(
                    color: Colors.black26,
                    thickness: 1.0,
                  )
                : SizedBox(
                    width: 1,
                  )
          ],
        ),
      ),
    );
  }
}
