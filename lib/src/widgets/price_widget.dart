import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';

class PriceWidget extends StatefulWidget {
  final String price;
  PriceWidget({this.price});
  @override
  _PriceWidgetState createState() => _PriceWidgetState();
}

class _PriceWidgetState extends State<PriceWidget> {
  @override
  Widget build(BuildContext context) {
    return Text(
      "Price \u20B9" + widget.price,
      textAlign: TextAlign.center,
      style: TextStyle(
          color: Constants.kitGradients[3],
          fontFamily: "PrompLight",
          fontSize: 20),
    );
  }
}
