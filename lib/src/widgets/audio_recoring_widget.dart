import 'dart:async';
import 'dart:io' as io;

import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/delivery_icon_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_audio_recorder2/flutter_audio_recorder2.dart';
import 'package:path_provider/path_provider.dart';
import 'package:file/file.dart';
import 'package:file/local.dart';

class AudioRecordingWidget extends StatefulWidget {
  final ValueChanged onChanged;
  AudioRecordingWidget({this.onChanged});
  @override
  _AudioRecordingWidgetState createState() => _AudioRecordingWidgetState();
}

class _AudioRecordingWidgetState extends State<AudioRecordingWidget> {
  RecordingStatus _currentStatus = RecordingStatus.Unset;
  FlutterAudioRecorder2 _recorder;
  Recording _current;
  LocalFileSystem localFileSystem = LocalFileSystem();
  File audioFile;

  // void setStatusTime() {
  //   const tick = const Duration(milliseconds: 50);
  //   new Timer.periodic(tick, (Timer t) async {
  //     if (_currentStatus == RecordingStatus.Stopped) {
  //       t.cancel();
  //     }
  //
  //     var current = await _recorder.current(channel: 0);
  //     // print(current.status);
  //     setState(() {
  //       _current = current;
  //       _currentStatus = _current.status;
  //     });
  //   });
  // }

  @override
  void dispose() {
    if (_currentStatus != RecordingStatus.Unset) {
      _recorder.stop();
    }
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 20)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          DeliveryIcon(
            color: Constants.kitGradients[0],
            title: _buildText(_currentStatus),
            icon: Icons.keyboard_voice_sharp,
            onPressed: () async {
              switch (_currentStatus) {
                case RecordingStatus.Initialized:
                  {
                    _start();
                    break;
                  }
                case RecordingStatus.Recording:
                  {
                    _currentStatus != RecordingStatus.Unset ? _stop() : null;
                    break;
                  }

                case RecordingStatus.Stopped:
                  {
                    await _init();
                    _start();
                    break;
                  }
                case RecordingStatus.Unset:
                  {
                    await _init();
                    _start();
                    break;
                  }
                default:
                  break;
              }
            },
          ),
          DeliveryIcon(
            color: Constants.kitGradients[0],
            title: "send",
            icon: Icons.send,
            onPressed: () async {
              if (_currentStatus != RecordingStatus.Unset) {
                _recorder.stop();
                widget.onChanged(audioFile);
                setState(() {});
              } else {}
            },
          ),
        ],
      ),
    );
  }

  _init() async {
    try {
      bool hasPermission = await FlutterAudioRecorder2.hasPermissions ?? false;

      if (hasPermission) {
        String customPath = '/flutter_audio_recorder';
        io.Directory appDocDirectory;

        if (io.Platform.isIOS) {
          appDocDirectory = await getApplicationDocumentsDirectory();
        } else {
          appDocDirectory = (await getExternalStorageDirectory());
        }

        // can add extension like ".mp4" ".wav" ".m4a" ".aac"
        customPath = appDocDirectory.path +
            customPath +
            DateTime.now().millisecondsSinceEpoch.toString() +
            ".mp4";

        _recorder = FlutterAudioRecorder2(
          customPath,
        );

        await _recorder.initialized;
        var current = await _recorder.current(channel: 0);
        print(current);
        // should be "Initialized", if all working fine
        setState(() {
          _current = current;
          _currentStatus = current.status;
          print(_currentStatus);
        });
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: new Text("You must accept permissions")));
      }
    } catch (e) {
      print(e);
    }
  }

  _start() async {
    try {
      await _recorder.start();
      var recording = await _recorder.current(channel: 0);
      setState(() {
        _current = recording;
        _currentStatus = _current.status;
        print(AudioFormat.AAC.index.toString());
      });
    } catch (e) {
      print(e);
    }
  }

  _stop() async {
    var result = await _recorder.stop();
    print("Stop recording: ${result.path}");
    print("Stop recording: ${result.duration}");
    audioFile = localFileSystem.file(result.path);
    print("File length: ${await audioFile.length()}");
    setState(() {
      _current = result;
      _currentStatus = _current.status;
    });
  }

  String _buildText(RecordingStatus status) {
    var text = "";
    switch (_currentStatus) {
      case RecordingStatus.Initialized:
        {
          text = 'Start';
          break;
        }
      case RecordingStatus.Recording:
        {
          text = 'Recording';
          break;
        }
      case RecordingStatus.Paused:
        {
          text = 'Resume';
          break;
        }
      case RecordingStatus.Stopped:
        {
          text = 'Stopped';
          break;
        }
      default:
        text = "Start";
        break;
    }
    return text;
  }

  // void onPlayAudio() async {
  //   AudioPlayer audioPlayer = AudioPlayer();
  //   await audioPlayer.play(_current.path, isLocal: true);
  // }
}
