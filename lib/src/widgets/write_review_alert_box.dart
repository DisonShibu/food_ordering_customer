import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/restuarant_button.dart';
import 'package:flutter/material.dart';

class WriteReviewAlertBox extends StatefulWidget {
  final String title;
  final double insetPadding;
  final double titlePadding;
  final double contentPadding;
  final Function onPressed;
  final TextEditingController reviewTextEditingController;
  WriteReviewAlertBox(
      {this.title,
      this.onPressed,
      this.contentPadding,
      this.insetPadding,
      this.titlePadding,
      this.reviewTextEditingController});
  @override
  _WriteReviewAlertBoxState createState() => _WriteReviewAlertBoxState();
}

class _WriteReviewAlertBoxState extends State<WriteReviewAlertBox> {
  FocusNode yourFocus = FocusNode();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.only(
          top: screenHeight(context,
              dividedBy: 3)), // adjust values according to your need),
      child: AlertDialog(
        // insetPadding: EdgeInsets.symmetric(
        //   vertical: screenHeight(context, dividedBy: widget.insetPadding),
        // ),
        titlePadding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: widget.titlePadding),
        ),
        contentPadding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: widget.contentPadding)),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(30),
          ),
        ),
        backgroundColor: Colors.white,
        content: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 3.5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        widget.title,
                        style: TextStyle(
                            fontFamily: "sfProSemiBold",
                            fontSize: 18,
                            color: Colors.black,
                            fontWeight: FontWeight.w600),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 40)),
                    child: Container(
                      height: screenHeight(context, dividedBy: 8),
                      child: TextField(
                        controller: widget.reviewTextEditingController,
                        cursorColor: Constants.kitGradients[5],
                        keyboardType: TextInputType.multiline,
                        focusNode: yourFocus,
                        onSubmitted: (value) {
                          setState(() {
                            yourFocus = FocusNode();
                          });
                        },
                        maxLines: 6,
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w400,
                          color: Constants.kitGradients[5],
                          fontFamily: "Prompt-Light",
                        ),
                        //readOnly: widget.readOnly,
                        onChanged: (value) {
                          setState(() {});
                        },
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: screenWidth(context, dividedBy: 40),
                              vertical: screenHeight(context, dividedBy: 80)),
                          hintText: "Type something",
                          hintStyle: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Constants.kitGradients[2],
                            fontFamily: "Prompt-Light",
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Constants.kitGradients[2], width: 1),
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Constants.kitGradients[2], width: 1),
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 150),
                  ),
                  RestaurantButton(
                    title: "SUBMIT",
                    onPressed: () {
                      widget.onPressed();
                      pop(context);
                    },
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
