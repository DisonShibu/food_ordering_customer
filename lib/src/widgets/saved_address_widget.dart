import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SavedAddressBox extends StatefulWidget {
  final Function onPressedAddress;
  final String heading;
  final String subHeading;
  final IconData icon;
  final String image;
  final bool svgIconTrue;
  final bool thirdLineTrue;
  final String thirdLine;
  final bool center;
  SavedAddressBox(
      {this.onPressedAddress,
      this.heading,
      this.subHeading,
      this.icon,
      this.image,
      this.thirdLineTrue,
      this.center,
      this.thirdLine,
      this.svgIconTrue});
  @override
  _SavedAddressBoxState createState() => _SavedAddressBoxState();
}

class _SavedAddressBoxState extends State<SavedAddressBox> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressedAddress();
      },
      child: Column(
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: widget.center == true
                  ? CrossAxisAlignment.center
                  : CrossAxisAlignment.start,
              children: [
                widget.svgIconTrue != true
                    ? Icon(
                        widget.icon,
                        size: screenWidth(context, dividedBy: 13),
                        color: Colors.black26,
                      )
                    : Image.asset(
                        widget.image,
                        width: screenWidth(context, dividedBy: 7),
                        height: screenHeight(context, dividedBy: 30),
                      ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 20),
                ),
                Column(
                  children: [
                    Container(
                      width: screenWidth(context, dividedBy: 1.3),
                      child: Text(
                        widget.heading,
                        style: TextStyle(
                            color: Constants.kitGradients[5],
                            fontFamily: "PrompLight",
                            fontSize: screenWidth(context, dividedBy: 20)),
                      ),
                    ),
                    Container(
                      width: screenWidth(context, dividedBy: 1.3),
                      child: Text(
                        widget.subHeading,
                        style: TextStyle(
                            color: Constants.kitGradients[5],
                            fontFamily: 'OpenSansRegular',
                            fontSize: screenWidth(context, dividedBy: 26)),
                      ),
                    ),
                    widget.thirdLineTrue == true
                        ? Container(
                            width: screenWidth(context, dividedBy: 1.3),
                            child: Text(
                              widget.thirdLine,
                              style: TextStyle(
                                  color: Constants.kitGradients[2],
                                  fontFamily: 'OpenSansRegular',
                                  fontSize:
                                      screenWidth(context, dividedBy: 26)),
                            ),
                          )
                        : Container()
                  ],
                )
              ],
            ),
          ),
          Divider(
            color: Colors.black26,
            thickness: 1.0,
          )
        ],
      ),
    );
  }
}
