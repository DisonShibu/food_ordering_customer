import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProductImages extends StatefulWidget {
  final String images;
  final int currentIndex;
  final Function onPressed;
  final int imgListLength;
  ProductImages(
      {this.images, this.currentIndex, this.onPressed, this.imgListLength});

  @override
  _ProductImagesState createState() => _ProductImagesState();
}

class _ProductImagesState extends State<ProductImages> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Container(
              height: screenHeight(context, dividedBy: 2),
              width: screenWidth(context, dividedBy: 1),
              decoration: BoxDecoration(
                  color: Colors.red, borderRadius: BorderRadius.circular(10)),
              child: Image.asset(
                widget.images,
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 15)),
            child: Column(
              children: [
                Spacer(
                  flex: 10,
                ),
                Center(
                  child: DotsIndicator(
                    dotsCount: widget.imgListLength,
                    position: widget.currentIndex.toDouble(),
                    axis: Axis.horizontal,
                    decorator: DotsDecorator(
                        activeColor: Constants.kitGradients[0],
                        color: Colors.white.withOpacity(.40),
                        shape: CircleBorder(),
                        size: Size(10, 10)),
                  ),
                ),
                Spacer(
                  flex: 1,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
