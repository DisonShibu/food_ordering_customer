import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BuildIconButton extends StatefulWidget {
  final String title;
  final Function onPressed;
  final String icon;
  final bool checkOut;
  final bool colorChange;
  final Color color;
  final String price;
  BuildIconButton(
      {this.title,
      this.onPressed,
      this.icon,
      this.checkOut,
      this.color,
      this.price,
      this.colorChange});

  @override
  _BuildIconButtonState createState() => _BuildIconButtonState();
}

class _BuildIconButtonState extends State<BuildIconButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 20),
      child: ElevatedButton.icon(
        onPressed: () {
          widget.onPressed();
        },
        style: ElevatedButton.styleFrom(
          primary: widget.colorChange == true
              ? widget.color
              : Constants.kitGradients[6],
          onPrimary: widget.colorChange == true
              ? widget.color
              : Constants.kitGradients[6],
          elevation: 9,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
        ),
        icon: Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 0),
          child: Container(
            child: widget.colorChange == true
                ? SvgPicture.asset(
                    widget.icon,
                    color: Constants.kitGradients[0],
                    width: screenWidth(context, dividedBy: 10),
                  )
                : SvgPicture.asset(
                    widget.icon,
                    width: screenWidth(context, dividedBy: 10),
                  ),
          ),
        ),
        label: widget.checkOut == true
            ? Container(
                width: screenWidth(context, dividedBy: 1.9),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      widget.title,
                      style: TextStyle(
                          color: Constants.kitGradients[0],
                          fontFamily: 'PromptLight',
                          fontSize: 16),
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    Text(
                      widget.price,
                      style: TextStyle(
                          color: Constants.kitGradients[0],
                          fontFamily: 'PromptLight',
                          fontSize: 16),
                    ),
                  ],
                ),
              )
            : Text(
                widget.title,
                style: TextStyle(
                    color: Constants.kitGradients[0],
                    fontFamily: 'PromptLight',
                    fontSize: 16),
              ),
      ),
    );
  }
}
