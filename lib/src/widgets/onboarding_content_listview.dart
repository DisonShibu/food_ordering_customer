import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class OnBoardingContentListView extends StatefulWidget {
  final String image;
  final String dataHead;
  final String head;
  final String buttonData;
  final Function onPressed;
  final int index;
  final String data;
  OnBoardingContentListView(
      {this.image,
      this.dataHead,
      this.head,
      this.buttonData,
      this.onPressed,
      this.data,
      this.index});
  @override
  _OnBoardingContentListViewState createState() =>
      _OnBoardingContentListViewState();
}

class _OnBoardingContentListViewState extends State<OnBoardingContentListView> {
  int currentPage = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: screenWidth(context, dividedBy: 1),
      // height: screenHeight(context, dividedBy: 1),
      child: Stack(
        children: [
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 2.5),
            color: Color(0xffD9E7FD),
          ),
          Positioned(
            top: screenHeight(context, dividedBy: 9),
            child:
                // SvgPicture.asset(widget.image)
                Container(
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(horizontal: 15),
              width: screenWidth(context, dividedBy: 1),

              color: Colors.transparent,
              child: SvgPicture.asset(widget.image),
              //  Image(
              //   image: AssetImage(widget.image),
              // ),
            ),
          ),
          Positioned(
            top: screenHeight(context, dividedBy: 1.9),
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(horizontal: 15),
              width: screenWidth(context, dividedBy: 1),
              child: Text(
                widget.head,
                style: TextStyle(
                    fontFamily: "SofiaProRegular",
                    fontSize: 31,
                    fontWeight: FontWeight.w500),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Positioned(
            top: screenHeight(context, dividedBy: 1.65),
            left: screenWidth(context, dividedBy: 15),
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(horizontal: 15),
              width: screenWidth(context, dividedBy: 1.1),
              height: screenHeight(context, dividedBy: 3),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.dataHead,
                    style: TextStyle(
                        fontFamily: "SofiaProRegular", fontSize: 16, height: 2),
                    textAlign: TextAlign.start,
                    overflow: TextOverflow.visible,
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  Text(
                    widget.data,
                    style: TextStyle(
                        fontFamily: "SofiaProRegular",
                        fontSize: 16,
                        height: 1.3),
                    textAlign: TextAlign.start,
                  )
                ],
              ),
            ),
          ),
          Positioned(
              top: screenHeight(context, dividedBy: 1.05),
              child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(horizontal: 15),
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 18),
                // color: Colors.blue,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        height: 10,
                        width: 10,
                        decoration: BoxDecoration(
                            color: widget.index == 0
                                ? Constants.kitGradients[2]
                                : Color(0xffEEEEEE),
                            borderRadius: BorderRadius.circular(5)),
                      ),
                      SizedBox(
                        width: screenWidth(context, dividedBy: 30),
                      ),
                      Container(
                        height: 10,
                        width: 10,
                        decoration: BoxDecoration(
                            color: widget.index == 1
                                ? Constants.kitGradients[2]
                                : Color(0xffEEEEEE),
                            borderRadius: BorderRadius.circular(5)),
                      ),
                      SizedBox(
                        width: screenWidth(context, dividedBy: 30),
                      ),
                      Container(
                        height: 10,
                        width: 10,
                        decoration: BoxDecoration(
                            color: widget.index == 2
                                ? Constants.kitGradients[2]
                                : Color(0xffEEEEEE),
                            borderRadius: BorderRadius.circular(5)),
                      ),
                      SizedBox(
                        width: screenWidth(context, dividedBy: 30),
                      ),
                      Container(
                        height: 10,
                        width: 10,
                        decoration: BoxDecoration(
                            color: widget.index == 3
                                ? Constants.kitGradients[2]
                                : Color(0xffEEEEEE),
                            borderRadius: BorderRadius.circular(5)),
                      ),
                    ]),
              )),
        ],
      ),
    );
  }
}
