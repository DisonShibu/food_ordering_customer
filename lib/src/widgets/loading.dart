import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loading extends StatelessWidget {
  final String message;
  Loading({this.message});
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          message,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.lightGreen,
            fontSize: 24,
          ),
        ),
        SizedBox(height: 24),
        Container(
            child: SpinKitFadingCircle(
              color: Colors.black,
              size: 30,
            )),
      ],
    );
  }
}
