import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/build_button.dart';
import 'package:app_template/src/widgets/restuarant_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class OnBoardingContent extends StatefulWidget {
  final String image;
  final String data;
  final String head;
  final String buttonData;
  final Function onPressed;
  final int index;
  OnBoardingContent(
      {this.image,
      this.data,
      this.head,
      this.buttonData,
      this.onPressed,
      this.index});
  @override
  _OnBoardingContentState createState() => _OnBoardingContentState();
}

class _OnBoardingContentState extends State<OnBoardingContent> {
  int currentPage = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      // height: screenHeight(context, dividedBy: 1),
      child: Stack(
        children: [
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: widget.index == 3
                ? screenHeight(context, dividedBy: 2)
                : screenHeight(context, dividedBy: 2.5),
            color: Constants.kitGradients[2],
          ),
          Positioned(
            top: screenHeight(context, dividedBy: 12),
            child:
                // SvgPicture.asset(widget.image)
                Container(
              alignment: Alignment.center,
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 4),

              color: Colors.transparent,
              child: Image.asset(widget.image),
              //  Image(
              //   image: AssetImage(widget.image),
              // ),
            ),
          ),
          Positioned(
            top: screenHeight(context, dividedBy: 1.9),
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(horizontal: 15),
              width: screenWidth(context, dividedBy: 1),
              child: Text(
                widget.head,
                style: TextStyle(
                    fontFamily: "SofiaProRegular",
                    color: Constants.kitGradients[5],
                    fontSize: screenWidth(context, dividedBy: 14),
                    fontWeight: FontWeight.w500),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Positioned(
            top: screenHeight(context, dividedBy: 1.85),
            left: screenWidth(context, dividedBy: 16),
            child: Column(
              children: [
                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  width: screenWidth(context, dividedBy: 1.2),
                  height: screenHeight(context, dividedBy: 4),
                  child: Text(
                    widget.data,
                    style: TextStyle(
                        fontFamily: "SofiaProRegular",
                        color: Constants.kitGradients[5],
                        fontSize: screenWidth(context, dividedBy: 25)),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
          Positioned(
              top: screenHeight(context, dividedBy: 1.35),
              child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(horizontal: 15),
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 18),
                // color: Colors.blue,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        height: 10,
                        width: 10,
                        decoration: BoxDecoration(
                            color: widget.index == 0
                                ? Constants.kitGradients[2]
                                : Color(0xffEEEEEE),
                            borderRadius: BorderRadius.circular(5)),
                      ),
                      SizedBox(
                        width: screenWidth(context, dividedBy: 30),
                      ),
                      Container(
                        height: 10,
                        width: 10,
                        decoration: BoxDecoration(
                            color: widget.index == 1
                                ? Constants.kitGradients[2]
                                : Color(0xffEEEEEE),
                            borderRadius: BorderRadius.circular(5)),
                      ),
                      SizedBox(
                        width: screenWidth(context, dividedBy: 30),
                      ),
                      // Container(
                      //   height: 10,
                      //   width: 10,
                      //   decoration: BoxDecoration(
                      //       color: widget.index == 2
                      //           ? Constants.kitGradients[2]
                      //           : Color(0xffEEEEEE),
                      //       borderRadius: BorderRadius.circular(5)),
                      // ),
                      // SizedBox(
                      //   width: screenWidth(context, dividedBy: 30),
                      // ),
                      // Container(
                      //   height: 10,
                      //   width: 10,
                      //   decoration: BoxDecoration(
                      //       color: widget.index == 3
                      //           ? Constants.kitGradients[2]
                      //           : Color(0xffEEEEEE),
                      //       borderRadius: BorderRadius.circular(5)),
                      // ),
                    ]),
              )),
          Positioned(
            top: screenHeight(context, dividedBy: 1.20),
            //left: screenWidth(context, dividedBy: 15),
            child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(horizontal: 15),
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 10),
                child: RestaurantButton(
                  title: widget.buttonData,
                  onPressed: widget.onPressed,
                )),
          ),
        ],
      ),
    );
  }
}
