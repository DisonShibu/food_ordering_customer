import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DeliveryIcon extends StatefulWidget {
  final IconData icon;
  final String title;
  final Color color;
  final Function onPressed;

  DeliveryIcon({this.icon, this.title, this.color, this.onPressed});

  @override
  _DeliveryIconState createState() => _DeliveryIconState();
}

class _DeliveryIconState extends State<DeliveryIcon> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: () {
            widget.onPressed();
          },
          child: Card(
            shape: CircleBorder(),
            margin: EdgeInsets.zero,
            child: Container(
              height: screenHeight(context, dividedBy: 13),
              width: screenWidth(context, dividedBy: 7),
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: Constants.kitGradients[2]),
              child: Icon(
                widget.icon,
                size: 30,
                color: widget.color,
              ),
            ),
          ),
        ),
        SizedBox(width: 2),
        Container(
          child: Text(
            widget.title,
            style: TextStyle(
              color: Constants.kitGradients[5],
              fontWeight: FontWeight.bold,
              fontFamily: "PrompLight",
              fontSize: screenWidth(context, dividedBy: 26),
            ),
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }
}
