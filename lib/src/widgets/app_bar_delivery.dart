import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class AppBarDelivery extends StatefulWidget {
  final String title;
  final Color color;
  final bool isWhite;
  final IconData leftIcon;
  final IconData rightIcon;
  final Function onPressedLeftIcon;
  final Function onPressedRightIcon;
  final bool rightIconTrue;
  final bool leftIconTrue;
  final double elevation;
  AppBarDelivery({
    this.title,
    this.color,
    this.isWhite,
    this.rightIcon,
    this.leftIcon,
    this.leftIconTrue,
    this.rightIconTrue,
    this.onPressedLeftIcon,
    this.onPressedRightIcon,
    this.elevation,
  });
  @override
  _AppBarDeliveryState createState() => _AppBarDeliveryState();
}

class _AppBarDeliveryState extends State<AppBarDelivery> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: widget.elevation != 0 ? 7 : 0,
      backgroundColor: widget.color,
      leading: widget.leftIconTrue != false
          ? GestureDetector(
              onTap: widget.onPressedLeftIcon,
              child: Icon(
                widget.leftIcon,
                size: 20,
                color: Colors.black,
              ),
            )
          : Container(),
      leadingWidth: screenWidth(context, dividedBy: 10),
      title: Row(
        children: [
          Text(
            widget.title,
            style: TextStyle(
                fontSize: screenWidth(context, dividedBy: 18),
                color: Colors.black,
                fontWeight: FontWeight.w400,
                fontFamily: "SofiaProRegular"),
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
      actions: [
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 20)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              widget.rightIconTrue == true
                  ? GestureDetector(
                      onTap: widget.onPressedRightIcon,
                      child: Icon(
                        widget.rightIcon,
                        size: 20,
                        color: Colors.black,
                      ),
                    )
                  : Container()
            ],
          ),
        )
      ],
    );
  }
}
