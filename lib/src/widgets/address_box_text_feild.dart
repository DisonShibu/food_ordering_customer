import 'dart:ui';

import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddressBoxTextFeild extends StatefulWidget {
  final String title;
  final IconData icon;
  final TextEditingController searchTextEditingController;
  AddressBoxTextFeild(
      {this.searchTextEditingController, this.title, this.icon});

  @override
  _AddressBoxTextFeildState createState() => _AddressBoxTextFeildState();
}

class _AddressBoxTextFeildState extends State<AddressBoxTextFeild> {
  @override
  Widget build(BuildContext context) {
    return TextField(
      cursorColor: Colors.black45,
      keyboardType: TextInputType.text,
      controller: widget.searchTextEditingController,
      selectionHeightStyle: BoxHeightStyle.tight,
      style: TextStyle(color: Colors.black, fontFamily: "OpenSansRegular"),
      decoration: InputDecoration(
        fillColor: Colors.grey,
        contentPadding: EdgeInsets.only(top: 14),
        prefixIcon: Icon(
          widget.icon,
          size: 30,
          color: Colors.black26,
        ),

        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.black26),
        ),
        // enabledBorder: OutlineInputBorder(
        //   borderSide: BorderSide(
        //       color: Colors.transparent.withOpacity(0.1), width: 0),
        //   borderRadius: BorderRadius.circular(10),
        // ),
        hintText: widget.title,
        hintStyle: TextStyle(color: Colors.black87, fontFamily: "PrompLight"),
      ),
    );
  }
}
