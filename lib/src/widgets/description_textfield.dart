import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class DescriptionTextField extends StatefulWidget {
  //const FoodDescriptionTextField({Key? key}) : super(key: key);
  final TextEditingController textEditingController;
  final String hintText;
  final Widget suffixIcon;
  final Function onTapIcon;
  final Function onPressed;
  final Function onChanged;
  final bool phoneNumber;
  DescriptionTextField(
      {this.textEditingController,
      this.hintText,
      this.suffixIcon,
      this.onChanged,
      this.onTapIcon,
      this.phoneNumber,
      this.onPressed});

  @override
  _DescriptionTextFieldState createState() => _DescriptionTextFieldState();
}

class _DescriptionTextFieldState extends State<DescriptionTextField> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          decoration: BoxDecoration(
              color: Constants.kitGradients[0],
              borderRadius: BorderRadius.circular(15)),
          // height: screenHeight(context, dividedBy: 15),
          child: TextField(
            onTap: () {
              widget.onPressed();
            },
            controller: widget.textEditingController,
            cursorColor: Constants.kitGradients[3],

            keyboardType: widget.phoneNumber == true
                ? TextInputType.number
                : TextInputType.text,
            obscureText: false,
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w400,
              color: Constants.kitGradients[3],
              fontFamily: "Prompt-Light",
            ),
            //readOnly: widget.readOnly,
            decoration: InputDecoration(
              //errorText: widget.errorText,
              suffixIcon: widget.suffixIcon == null
                  ? Container(width: screenWidth(context, dividedBy: 90))
                  : GestureDetector(
                      onTap: widget.onTapIcon, child: widget.suffixIcon),

              contentPadding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 50),
                  vertical: screenHeight(context, dividedBy: 120)),

              hintText: widget.hintText,
              hintStyle: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: Constants.kitGradients[5],
                fontFamily: "PrompLight",
              ),
              fillColor: Colors.white,
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.white,
                ),
                borderRadius: BorderRadius.circular(20),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Constants.kitGradients[1],
                ),
                borderRadius: BorderRadius.circular(15),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
