import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListTileFoodCount extends StatefulWidget {
  final String foodname;
  final String foodcount;
  final bool veg;
  ListTileFoodCount({this.foodname, this.foodcount, this.veg});

  @override
  _ListTileFoodCountState createState() => _ListTileFoodCountState();
}

class _ListTileFoodCountState extends State<ListTileFoodCount> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 20)),
      child: Row(
        children: [
          Icon(
            Icons.radio_button_checked,
            size: 20,
            color: Colors.red,
          ),
          SizedBox(
            width: screenWidth(context, dividedBy: 20),
          ),
          Container(
            width: screenWidth(context, dividedBy: 2),
            child: Text(
              widget.foodcount + " x " + widget.foodname,
              style: TextStyle(
                  color: Constants.kitGradients[5],
                  fontSize: screenWidth(context, dividedBy: 20),
                  fontFamily: "PrompLight"),
            ),
          ),
        ],
      ),
    );
    ;
  }
}
