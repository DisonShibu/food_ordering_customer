import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/cart_page_image_tile.dart';
import 'package:flutter/material.dart';

class PlaceOrderFoodTile extends StatefulWidget {
  final String image;
  final String restaurantName;
  final String foodName;
  final int quantity;
  final String price;
  PlaceOrderFoodTile(
      {this.price,
      this.restaurantName,
      this.foodName,
      this.image,
      this.quantity});

  @override
  _PlaceOrderFoodTileState createState() => _PlaceOrderFoodTileState();
}

class _PlaceOrderFoodTileState extends State<PlaceOrderFoodTile> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CartPageImageTile(
          image: widget.image,
          restaurantName: widget.restaurantName,
          foodName: widget.foodName,
          containerHeight: 7,
          containerWidth: 1.2,
          clearButtonFalse: false,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                Text(
                  "#QTY",
                  style: TextStyle(
                    color: Constants.kitGradients[2],
                    fontWeight: FontWeight.w400,
                    fontSize: 18,
                    fontFamily: "OswaldRe",
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  widget.quantity.toString(),
                  style: TextStyle(
                    color: Constants.kitGradients[2],
                    fontWeight: FontWeight.w400,
                    fontSize: 18,
                    fontFamily: "OswaldRe",
                  ),
                ),
              ],
            ),
            Column(
              children: [
                Text(
                  "Total Price",
                  style: TextStyle(
                    color: Constants.kitGradients[2],
                    fontWeight: FontWeight.w400,
                    fontSize: 18,
                    fontFamily: "OswaldRe",
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  widget.price,
                  style: TextStyle(
                    color: Constants.kitGradients[2],
                    fontWeight: FontWeight.w400,
                    fontSize: 18,
                    fontFamily: "OswaldRe",
                  ),
                ),
              ],
            ),
          ],
        ),
        Divider(
          color: Colors.black54,
        ),
      ],
    );
  }
}
