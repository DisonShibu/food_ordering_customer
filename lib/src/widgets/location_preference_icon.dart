import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LocationPreferenceIcon extends StatefulWidget {
  final IconData icon;
  final String title;
  final Color color;
  LocationPreferenceIcon({this.icon, this.title, this.color});
  @override
  _LocationPreferenceIconState createState() => _LocationPreferenceIconState();
}

class _LocationPreferenceIconState extends State<LocationPreferenceIcon> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Card(
          margin: EdgeInsets.zero,
          child: Icon(
            widget.icon,
            size: 30,
            color: widget.color,
          ),
        ),
        SizedBox(width: 2),
        Container(
          child: Text(
            widget.title,
            style: TextStyle(
              color: widget.color,
              fontWeight: FontWeight.bold,
              fontFamily: "PrompLight",
              fontSize: screenWidth(context, dividedBy: 26),
            ),
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
    ;
  }
}
