import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/material.dart';

typedef void RatingChangeCallback(double rating);

class StarRatingBar extends StatelessWidget {
  final int starCount;
  final double rating;
  final RatingChangeCallback onRatingChanged;
  final Color color;
  final double size;

  StarRatingBar(
      {this.starCount = 5,
      this.rating = .0,
      this.onRatingChanged,
      this.color,
      this.size = 20});

  Widget buildStar(BuildContext context, int index, double size) {
    Icon icon;

    if (index > rating) {
      icon = new Icon(
        Icons.star_border,
        size: size,
        color: Constants.kitGradients[2],
      );
    } else {
      icon = new Icon(
        Icons.star,
        size: size,
        color: color ?? Constants.kitGradients[2],
      );
    }
    return new GestureDetector(
      onTap: () {
        if (onRatingChanged == null) {
        } else {
          if (rating >= index) {
            onRatingChanged(index - 1.0);
          } else {
            onRatingChanged(index.toDouble());
          }
        }
      },
      child: icon,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: new List.generate(
                starCount, (index) => buildStar(context, index, size))));
  }
}
