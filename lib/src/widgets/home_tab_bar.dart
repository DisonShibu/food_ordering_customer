import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../utils/utils.dart';

class HomeTabBar extends StatefulWidget {
  final Function onTapCart;
  final Function onTapHome;
  final Function onTapSearch;
  final double currentIndex;
  final Function onTapOrder;
  HomeTabBar(
      {this.onTapHome,
      this.onTapCart,
      this.onTapOrder,
      this.currentIndex,
      this.onTapSearch});
  @override
  _HomeTabBarState createState() => _HomeTabBarState();
}

class _HomeTabBarState extends State<HomeTabBar> {
  int count;
  int _currentIndex = 0;
  bool expanded = true;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 12),
      padding: EdgeInsets.symmetric(
        horizontal: screenWidth(context, dividedBy: 15),
      ),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Constants.kitGradients[0],
        border: Border(
          bottom: BorderSide(color: Constants.kitGradients[0]),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          // SizedBox(
          //   width: screenWidth(context, dividedBy: 10),
          // ),
          GestureDetector(
            onTap: () {
              widget.onTapHome();
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                widget.currentIndex == 0
                    ? Icon(
                        Icons.home,
                        size: 32,
                        color: Constants.kitGradients[2],
                      )
                    : Icon(
                        Icons.home_outlined,
                        size: 32,
                        color: Constants.kitGradients[2],
                      ),
                Text(
                  "HOME",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 13,
                      fontFamily: "PrompLight",
                      fontWeight: FontWeight.w600),
                )
              ],
            ),
          ),
          Spacer(),
          GestureDetector(
            onTap: () {
              widget.onTapSearch();
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                widget.currentIndex == 1
                    ? Icon(
                        Icons.search,
                        size: 30,
                        color: Constants.kitGradients[2],
                      )
                    : Icon(
                        Icons.search,
                        size: 30,
                        color: Constants.kitGradients[2],
                      ),
                Text(
                  "SEARCH",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 13,
                      fontFamily: "PrompLight",
                      fontWeight: FontWeight.w600),
                )
              ],
            ),
          ),
          Spacer(),
          GestureDetector(
            onTap: () {
              widget.onTapOrder();
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                widget.currentIndex == 2
                    ? Icon(Icons.shopping_bag,
                        color: Constants.kitGradients[2], size: 30)
                    : Icon(Icons.shopping_bag_outlined,
                        color: Constants.kitGradients[2], size: 30),
                Text(
                  "ORDERS",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 13,
                      fontFamily: "PrompLight",
                      fontWeight: FontWeight.w600),
                )
              ],
            ),
          ),
          Spacer(),
          GestureDetector(
            onTap: () {
              widget.onTapCart();
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                widget.currentIndex == 3
                    ? Icon(Icons.shopping_cart,
                        color: Constants.kitGradients[2], size: 30)
                    : Icon(Icons.shopping_cart_outlined,
                        color: Constants.kitGradients[2], size: 30),
                Text(
                  "CART",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 13,
                      fontFamily: "PrompLight",
                      fontWeight: FontWeight.w600),
                )
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
