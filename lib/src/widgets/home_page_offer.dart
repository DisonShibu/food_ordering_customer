import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomePageOffer extends StatefulWidget {
  final String images;
  final int currentIndex;
  final Function onPressed;
  final int imgListLength;
  HomePageOffer(
      {this.images, this.currentIndex, this.onPressed, this.imgListLength});

  @override
  _HomePageOfferState createState() => _HomePageOfferState();
}

class _HomePageOfferState extends State<HomePageOffer> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 20)),
      child: GestureDetector(
        onTap: () {
          widget.onPressed();
        },
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Stack(
            children: [
              Image.asset(
                widget.images,
                fit: BoxFit.fill,
                height: screenHeight(context, dividedBy: 1),
                width: screenWidth(context, dividedBy: 1),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 15)),
                child: Column(
                  children: [
                    Spacer(
                      flex: 5,
                    ),
                    Row(
                      children: [
                        Container(
                            width: screenWidth(context, dividedBy: 4),
                            child: Text(
                              "Adams Apple 20 %o off",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 13,
                                  fontFamily: "PrompLight",
                                  fontWeight: FontWeight.w600),
                            )),
                      ],
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    Row(
                      children: [
                        ElevatedButton.icon(
                          icon: Icon(
                            Icons.double_arrow_rounded,
                            color: Colors.white,
                          ),
                          onPressed: () {},
                          label: Text(
                            "Go",
                            style: TextStyle(
                                color: Constants.kitGradients[0],
                                fontFamily: "MerriWeather"),
                          ),
                          style: ElevatedButton.styleFrom(
                              elevation: 2,
                              primary: Constants.kitGradients[6],
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10))),
                        ),
                      ],
                    ),
                    Spacer(
                      flex: 2,
                    ),
                    Center(
                      child: DotsIndicator(
                        dotsCount: widget.imgListLength,
                        position: widget.currentIndex.toDouble(),
                        axis: Axis.horizontal,
                        decorator: DotsDecorator(
                            activeColor: Constants.kitGradients[0],
                            color: Colors.white.withOpacity(.40),
                            shape: CircleBorder(),
                            size: Size(10, 10)),
                      ),
                    ),
                    Spacer(
                      flex: 1,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
