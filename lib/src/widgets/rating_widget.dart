import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/heading_widget.dart';
import 'package:flutter/cupertino.dart';

class ReviewWidget extends StatefulWidget {
  final String reviewTitle;
  final String body;
  final String rating;
  ReviewWidget({this.body, this.reviewTitle, this.rating});
  @override
  _ReviewWidgetState createState() => _ReviewWidgetState();
}

class _ReviewWidgetState extends State<ReviewWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // SizedBox(
          //   height: screenHeight(context, dividedBy: 30),
          // ),
          Text(
            "Dison" + "",
            style: TextStyle(
              color: Constants.kitGradients[5],
              fontWeight: FontWeight.w500,
              fontSize: 17,
              fontFamily: "OswaldRegular",
            ),
          ),
          Text(
            widget.reviewTitle + "",
            style: TextStyle(
              color: Constants.kitGradients[5],
              fontWeight: FontWeight.w500,
              fontSize: 17,
              fontFamily: "OswaldRegular",
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 100),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Text(
                  widget.body,
                  // "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    fontSize: 15,
                    fontFamily: "OpenSansRegular",
                    color: Constants.kitGradients[5],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 30),
          ),
        ],
      ),
    );
  }
}
