import 'dart:developer';

import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CircularContainerWidget extends StatefulWidget {
  final IconData icon;
  final double radius;
  final Function onPressed;
  final Color iconColor;
  final Color boxColor;

  CircularContainerWidget(
      {this.onPressed, this.icon, this.radius, this.boxColor, this.iconColor});

  @override
  _CircularContainerWidgetState createState() =>
      _CircularContainerWidgetState();
}

class _CircularContainerWidgetState extends State<CircularContainerWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Card(
        shape: CircleBorder(),
        margin: EdgeInsets.zero,
        child: Container(
          height: screenHeight(context, dividedBy: widget.radius),
          width: screenWidth(context, dividedBy: 7),
          decoration:
              BoxDecoration(shape: BoxShape.circle, color: widget.boxColor),
          child: Icon(
            widget.icon,
            size: 30,
            color: widget.iconColor,
          ),
        ),
      ),
    );
  }
}
