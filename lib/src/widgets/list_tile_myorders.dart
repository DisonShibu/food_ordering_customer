import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

// ignore: must_be_immutable
class ListTileMyOrders extends StatefulWidget {
  final String foodImage;
  final String name;
  final String orderNum;
  final String address;
  final bool orderNumber;
  final bool isPrice;
  final double doubleWidth;
  final double doubleHeight;
  final bool cancelled;
  final String cancelledStatus;
  final String price;
  final bool textCenter;
  ListTileMyOrders(
      {this.foodImage,
      this.name,
      this.orderNum,
      this.isPrice,
      this.address,
      this.cancelled,
      this.cancelledStatus,
      this.textCenter,
      this.price,
      this.doubleWidth,
      this.doubleHeight,
      this.orderNumber});
  @override
  _ListTileMyOrdersState createState() => _ListTileMyOrdersState();
}

class _ListTileMyOrdersState extends State<ListTileMyOrders> {
  @override
  void initState() {
    print(widget.doubleHeight);
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 40)),
      child: Card(
        color: Constants.kitGradients[0],
        elevation: 3,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        child: Container(
          width: screenWidth(context, dividedBy: 1),
          height: screenHeight(context, dividedBy: 7),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: screenWidth(context,
                    dividedBy:
                        widget.doubleWidth == null ? 3.2 : widget.doubleWidth),
                height: screenHeight(context,
                    dividedBy:
                        widget.doubleHeight == null ? 7 : widget.doubleHeight),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: Image.network(
                    widget.foodImage,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              widget.doubleWidth != null
                  ? SizedBox(
                      width: screenWidth(context, dividedBy: 40),
                    )
                  : SizedBox(),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 50)),
                child: Container(
                  height: screenHeight(context, dividedBy: 7),
                  width: screenWidth(context, dividedBy: 2.4),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: widget.textCenter != true
                        ? MainAxisAlignment.start
                        : MainAxisAlignment.center,
                    children: [
                      Text(
                        widget.name,
                        style: TextStyle(
                            color: Constants.kitGradients[3],
                            fontFamily: "PrompLight",
                            fontSize: screenWidth(context, dividedBy: 20)),
                      ),
                      widget.orderNumber != false
                          ? Text(
                              widget.orderNum,
                              style: TextStyle(
                                  color: Constants.kitGradients[3],
                                  fontFamily: "PrompLight",
                                  fontSize:
                                      screenWidth(context, dividedBy: 35)),
                            )
                          : Container(),
                      Text(
                        widget.address,
                        style: TextStyle(
                            color: Constants.kitGradients[3],
                            fontFamily: "PrompLight",
                            fontSize: screenWidth(context, dividedBy: 35)),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          widget.isPrice != false
                              ? Text(
                                  "Price",
                                  style: TextStyle(
                                      color: Constants.kitGradients[3],
                                      fontFamily: "PrompLight",
                                      fontSize:
                                          screenWidth(context, dividedBy: 26)),
                                )
                              : Container(),
                          Text(
                            widget.price + " SR",
                            style: TextStyle(
                                color: Constants.kitGradients[3],
                                fontFamily: "PrompLight",
                                fontSize: screenWidth(context, dividedBy: 29)),
                          ),
                        ],
                      ),
                    ),
                    widget.cancelled == true
                        ? Card(
                            margin: EdgeInsets.zero,
                            elevation: 3,
                            child: Container(
                              width: screenWidth(context, dividedBy: 6),
                              child: Center(
                                child: Text(
                                  widget.cancelledStatus,
                                  style: TextStyle(
                                      color: Constants.kitGradients[3],
                                      fontFamily: "PrompLight",
                                      fontSize:
                                          screenWidth(context, dividedBy: 37)),
                                ),
                              ),
                            ),
                          )
                        : Container()
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
