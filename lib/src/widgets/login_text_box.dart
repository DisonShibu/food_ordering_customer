import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginTextBox extends StatefulWidget {
  IconData icon;
  TextEditingController controller;
  String hintText;
  bool phoneNumber;
  LoginTextBox({this.controller, this.icon, this.hintText, this.phoneNumber});

  @override
  _LoginTextBoxState createState() => _LoginTextBoxState();
}

class _LoginTextBoxState extends State<LoginTextBox> {
  @override
  Widget build(BuildContext context) {
    return TextField(
      keyboardType: widget.phoneNumber == true
          ? TextInputType.number
          : TextInputType.text,
      style: TextStyle(
          color: Constants.kitGradients[5], fontFamily: "OpenSansRegular"),
      controller: widget.controller,
      decoration: InputDecoration(
          prefixIcon: Icon(
            widget.icon,
            size: 20,
            color: Constants.kitGradients[3],
          ),
          focusedBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: Constants.kitGradients[3], width: 2.0),
            borderRadius: BorderRadius.circular(25.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: Constants.kitGradients[3], width: 2.0),
            borderRadius: BorderRadius.circular(25.0),
          ),
          hintText: widget.hintText,
          hintStyle: TextStyle(color: Constants.kitGradients[3]),
          border: InputBorder.none),
    );
  }
}
