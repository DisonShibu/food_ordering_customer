import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/restuarant_button.dart';
import 'package:app_template/src/widgets/review_button.dart';
import 'package:flutter/material.dart';

class CancelAlertBox extends StatefulWidget {
  final String title;
  final double insetPadding;
  final double titlePadding;
  final double contentPadding;
  final Function onPressedYes;
  final Function onPressedNo;
  CancelAlertBox({
    this.title,
    this.onPressedYes,
    this.onPressedNo,
    this.contentPadding,
    this.insetPadding,
    this.titlePadding,
  });
  @override
  _CancelAlertBoxState createState() => _CancelAlertBoxState();
}

class _CancelAlertBoxState extends State<CancelAlertBox> {
  FocusNode yourFocus = FocusNode();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.only(
          top: screenHeight(context,
              dividedBy: 3)), // adjust values according to your need),
      child: AlertDialog(
        // insetPadding: EdgeInsets.symmetric(
        //   vertical: screenHeight(context, dividedBy: widget.insetPadding),
        // ),
        titlePadding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: widget.titlePadding),
        ),
        contentPadding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: widget.contentPadding)),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(30),
          ),
        ),
        backgroundColor: Colors.white,
        content: Container(
          width: screenWidth(context, dividedBy: 1),
          height: screenHeight(context, dividedBy: 3.5),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Spacer(
                flex: 3,
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 10)),
                child: Container(
                  height: screenHeight(context, dividedBy: 8),
                  child: Text(
                    widget.title,
                    style: TextStyle(
                        color: Constants.kitGradients[5],
                        fontFamily: "OpenSansRegular",
                        fontSize: screenWidth(context, dividedBy: 20)),
                  ),
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 150),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: screenWidth(context, dividedBy: 4),
                    child: ReviewButton(
                      title: "Yes",
                      onPressed: () {
                        widget.onPressedYes();
                      },
                    ),
                  ),
                  Container(
                    width: screenWidth(context, dividedBy: 4),
                    child: ReviewButton(
                      title: "No",
                      onPressed: () {
                        widget.onPressedNo();
                        pop(context);
                      },
                    ),
                  ),
                ],
              ),
              Spacer(
                flex: 1,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
