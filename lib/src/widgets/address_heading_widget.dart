import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';

class AddressHeadingWidget extends StatefulWidget {
  String title;
  AddressHeadingWidget({this.title});
  @override
  _AddressHeadingWidgetState createState() => _AddressHeadingWidgetState();
}

class _AddressHeadingWidgetState extends State<AddressHeadingWidget> {
  @override
  Widget build(BuildContext context) {
    return Text(
      widget.title + ":",
      style: TextStyle(
        color: Constants.kitGradients[5],
        fontWeight: FontWeight.w400,
        fontSize: 18,
        fontFamily: "OswaldRegular",
      ),
    );
  }
}
