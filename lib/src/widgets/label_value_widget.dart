import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class LabelAndValueWidget extends StatefulWidget {
  final String label;
  final String value;
  final bool iconTrue;
  LabelAndValueWidget({this.label, this.value, this.iconTrue});

  @override
  _LabelAndValueWidgetState createState() => _LabelAndValueWidgetState();
}

class _LabelAndValueWidgetState extends State<LabelAndValueWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: screenWidth(context, dividedBy: 2.3),
            alignment: Alignment.centerLeft,
            child: widget.iconTrue == true
                ? Image.asset(
                    widget.label,
                    fit: BoxFit.fitHeight,
                  )
                : Text(
                    widget.label,
                    style: TextStyle(
                      color: Constants.kitGradients[5],
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                      fontFamily: "OswaldRe",
                    ),
                  ),
          ),
          Container(
            width: screenWidth(context, dividedBy: 2.4),
            alignment: Alignment.centerRight,
            child: Text(
              widget.value,
              style: TextStyle(
                color: Constants.kitGradients[5],
                fontWeight: FontWeight.w400,
                fontSize: 18,
                fontFamily: "OswaldRe",
              ),
            ),
          ),
        ],
      ),
    );
  }
}
