import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DropDownFormField extends StatefulWidget {
  final List<String> dropDownList;
  final String title;
  final String dropDownValue;
  final bool underline;
  final bool hintText;
  final double boxWidth;
  final double boxHeight;
  final double textBoxWidth;
  final String fontFamily;
  final double hintPadding;
  final String hintFontFamily;
  final bool centreAlignText;
  final bool underLineFalse;
  ValueChanged<String> onClicked;
  DropDownFormField(
      {this.fontFamily,
      this.hintText,
      this.title,
      this.dropDownList,
      this.hintPadding,
      this.dropDownValue,
      this.onClicked,
      this.underline,
      this.boxWidth,
      this.boxHeight,
      this.textBoxWidth,
      this.centreAlignText,
      this.underLineFalse,
      this.hintFontFamily});
  @override
  _DropDownFormFieldState createState() => _DropDownFormFieldState();
}

String dropdownValue;

class _DropDownFormFieldState extends State<DropDownFormField> {
  @override
  void initState() {
    // TODO: implement initState
    print(widget.dropDownList);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(
          height: screenHeight(context, dividedBy: widget.boxHeight),
          width: screenWidth(context, dividedBy: widget.boxWidth),
          color: Colors.transparent,
          child: DropdownButton<String>(
              underline: Divider(
                thickness: screenHeight(context, dividedBy: 760),
                color: widget.underLineFalse == false
                    ? Colors.transparent
                    : Colors.grey,
              ),
              dropdownColor: Constants.kitGradients[0],
              value: widget.dropDownValue == ""
                  ? dropdownValue
                  : widget.dropDownValue,
              style: TextStyle(
                  fontSize: 18,
                  color: Constants.kitGradients[0],
                  fontFamily: 'SFProText-Regular',
                  fontWeight: FontWeight.w600),
              items: widget.dropDownList
                  .map<DropdownMenuItem<String>>(
                      (String value) => DropdownMenuItem<String>(
                          value: value,
                          child: Container(
                            width: screenWidth(context,
                                dividedBy: widget.textBoxWidth),
                            child: Row(
                              children: [
                                Text(
                                  value,
                                  style: TextStyle(
                                    color: Constants.kitGradients[5],
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: widget.fontFamily,
                                  ),
                                ),
                              ],
                            ),
                            alignment: widget.centreAlignText != true
                                ? Alignment.centerRight
                                : Alignment.center,
                          )))
                  .toList(),
              onChanged: (selectedValue) {
                setState(() {});
                widget.onClicked(selectedValue);
              },
              hint: Container(
                child: Row(
                  children: [
                    SizedBox(
                      width:
                          screenWidth(context, dividedBy: widget.hintPadding),
                    ),
                    Text(
                      widget.title,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontFamily: widget.hintFontFamily),
                    ),
                  ],
                ),
              )),
        ),
      ],
    );
  }
}
