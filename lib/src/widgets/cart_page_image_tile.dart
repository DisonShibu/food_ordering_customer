import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CartPageImageTile extends StatefulWidget {
  final String restaurantName;
  final String foodName;
  final double containerHeight;
  final double containerWidth;
  final String image;
  final bool clearButtonFalse;
  final Function onPressedClear;
  CartPageImageTile(
      {this.restaurantName,
      this.foodName,
      this.containerHeight,
      this.containerWidth,
      this.image,
      this.onPressedClear,
      this.clearButtonFalse});

  @override
  _CartPageImageTileState createState() => _CartPageImageTileState();
}

class _CartPageImageTileState extends State<CartPageImageTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: widget.containerHeight),
      child: Row(
        children: [
          Container(
            width: screenWidth(context, dividedBy: 4),
            height: screenHeight(context, dividedBy: 9),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.network(
                widget.image,
                fit: BoxFit.fill,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 50)),
            child: Container(
              height: screenHeight(context, dividedBy: 7),
              width: screenWidth(context, dividedBy: 2.4),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    widget.foodName,
                    style: TextStyle(
                        color: Constants.kitGradients[3],
                        fontFamily: "PrompLight",
                        fontSize: screenWidth(context, dividedBy: 26)),
                  ),
                  Text(
                    widget.restaurantName,
                    style: TextStyle(
                        color: Constants.kitGradients[3],
                        fontFamily: "PrompLight",
                        fontSize: screenWidth(context, dividedBy: 35)),
                  ),
                ],
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              widget.clearButtonFalse != false
                  ? GestureDetector(
                      onTap: widget.onPressedClear,
                      child: Container(
                        height: screenHeight(context, dividedBy: 25),
                        width: screenWidth(context, dividedBy: 6),
                        decoration: BoxDecoration(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.circular(5),
                            border:
                                Border.all(color: Constants.kitGradients[5])),
                        child: Center(
                          child: Text(
                            "Clear",
                            style: TextStyle(
                                color: Constants.kitGradients[3],
                                fontFamily: "OswaldRegular",
                                fontSize: screenWidth(context, dividedBy: 25)),
                          ),
                        ),
                      ),
                    )
                  : Container(),
              SizedBox(
                height: screenHeight(context, dividedBy: 50),
              )
            ],
          ),
        ],
      ),
    );
  }
}
