import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EditProfile extends StatefulWidget {
  final String title;
  final IconData icons;
  final Function onPressed;

  EditProfile({this.icons, this.title, this.onPressed});

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Row(
        children: [
          Container(
            width: screenWidth(context, dividedBy: 1.5),
            height: screenHeight(context, dividedBy: 15),
            decoration: BoxDecoration(
                color: Constants.kitGradients[2].withOpacity(0.2),
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: Colors.grey)),
            child: Center(
              child: Text(
                widget.title,
                style: TextStyle(
                    fontSize: screenWidth(context, dividedBy: 22),
                    color: Colors.black,
                    fontWeight: FontWeight.w400,
                    fontFamily: "PrompLight"),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
          Spacer(
            flex: 1,
          ),
          Container(
            height: screenHeight(context, dividedBy: 15),
            width: screenWidth(context, dividedBy: 8),
            decoration: BoxDecoration(
                color: Constants.kitGradients[2].withOpacity(0.2),
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: Colors.grey)),
            child: Icon(
              widget.icons,
              size: 40,
              color: Constants.kitGradients[8],
            ),
          ),
        ],
      ),
    );
  }
}
