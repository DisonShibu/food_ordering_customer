import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/state/app_state.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/address_box_location.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/location_preference_icon.dart';
import 'package:app_template/src/widgets/location_text_box.dart';
import 'package:app_template/src/widgets/restuarant_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

class AddressAddingPage extends StatefulWidget {
  final String location;
  final LatLng latLng;
  AddressAddingPage({this.location, this.latLng});
  @override
  _AddressAddingPageState createState() => _AddressAddingPageState();
}

class _AddressAddingPageState extends State<AddressAddingPage> {
  int count = 0;
  List<String> tags = ["Home", "Work", "Other"];
  TextEditingController houseTextEditingController =
      new TextEditingController();
  TextEditingController landMarkTextEditingController =
      new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 13)),
          child: AppBarDelivery(
            title: "Enter the Details",
            elevation: 0,
            onPressedLeftIcon: () {
              pop(context);
            },
            color: Constants.kitGradients[0],
            leftIcon: Icons.arrow_back_ios,
            leftIconTrue: true,
            isWhite: true,
          )),
      body: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 20)),
          child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 100),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 20)),
              child: Row(
                children: [
                  Text(
                    "Your Location",
                    style: TextStyle(
                      color: Constants.kitGradients[5],
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                      fontFamily: "OswaldRegular",
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 80),
            ),
            Row(
              children: [
                LocationTextBox(
                  onPressed: () {},
                  icon: Icons.location_on_outlined,
                  padding: 20,
                  dividerTrue: false,
                  heading: widget.location,
                  boxWidth: 1.5,
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 80),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 20)),
              child: Row(
                children: [
                  Text(
                    "Select location",
                    style: TextStyle(
                      color: Constants.kitGradients[5],
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                      fontFamily: "OswaldRegular",
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 100),
            ),
            AddressLocationBox(
              hintText: "House/Flat No",
              subHeading: "",
              icon: Icons.home_outlined,
              onPressedAddress: () {},
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 100),
            ),
            AddressLocationBox(
              hintText: "LandMark (Optional) ",
              subHeading: "",
              icon: Icons.ac_unit,
              onPressedAddress: () {},
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 100),
            ),
            // AddressLocationBox(
            //   hintText: "Pincode",
            //   subHeading: "",
            //   icon: Icons.ac_unit,
            //   onPressedAddress: () {},
            // ),
            // SizedBox(
            //   height: screenHeight(context, dividedBy: 60),
            // ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 20)),
              child: Row(
                children: [
                  Text(
                    "Tag your Location",
                    style: TextStyle(
                      color: Constants.kitGradients[5],
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                      fontFamily: "OswaldRegular",
                    ),
                  )
                ],
              ),
            ),

            SizedBox(
              height: screenHeight(context, dividedBy: 100),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                  onTap: () {
                    count = 1;
                    setState(() {});
                  },
                  child: LocationPreferenceIcon(
                    color: count == 1 ? Constants.kitGradients[1] : Colors.grey,
                    icon: Icons.home_outlined,
                    title: "Home",
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    count = 2;
                    setState(() {});
                  },
                  child: LocationPreferenceIcon(
                    color: count == 2 ? Constants.kitGradients[1] : Colors.grey,
                    icon: Icons.work_outline_sharp,
                    title: "Work",
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    count = 3;
                    setState(() {});
                  },
                  child: LocationPreferenceIcon(
                    color: count == 3 ? Constants.kitGradients[1] : Colors.grey,
                    icon: Icons.location_city_outlined,
                    title: "Other",
                  ),
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),
            RestaurantButton(
              title: "SAVE ADDRESS",
              onPressed: () {
                push(context, HomePage());
                if (houseTextEditingController.text != null && count != 0) {
                  Provider.of<HomeState>(context, listen: false)
                      .locationValueStored(
                          locationCoordinates: Position(
                              latitude: widget.latLng.latitude,
                              longitude: widget.latLng.longitude),
                          value: widget.location);
                  ObjectFactory()
                      .appHive
                      .putUserLongitude(widget.latLng.longitude.toString());
                  ObjectFactory()
                      .appHive
                      .putUserLongitude(widget.latLng.latitude.toString());
                } else {
                  showToast("Please Enter all the details");
                }
              },
            )
          ])),
    );
  }
}
