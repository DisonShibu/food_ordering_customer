import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class AddAthleteNumber extends StatefulWidget {
  int count;
  Function decrementOnTap;
  Function incrementOnTap;
  AddAthleteNumber({this.count, this.incrementOnTap, this.decrementOnTap});
  @override
  _AddAthleteNumberState createState() => _AddAthleteNumberState();
}

class _AddAthleteNumberState extends State<AddAthleteNumber> {
  int count;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Constants.kitGradients[0],
          border: Border.all(color: Constants.kitGradients[10]),
          borderRadius: BorderRadius.circular(10)),
      width: screenWidth(context, dividedBy: 4),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              widget.count > 0
                  ? GestureDetector(
                      onTap: widget.decrementOnTap,
                      child: Icon(Icons.remove,
                          size: 17, color: Constants.kitGradients[10]))
                  : Icon(Icons.remove, size: 17, color: Colors.grey),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    widget.count.toString(),
                    style: TextStyle(
                        color: Constants.kitGradients[5], fontSize: 18),
                  ),
                  Container(
                    width: screenWidth(context, dividedBy: 10),
                    height: 2,
                    color: Colors.white10,
                  ),
                ],
              ),
              widget.count < 10
                  ? GestureDetector(
                      onTap: widget.incrementOnTap,
                      child: Icon(Icons.add,
                          size: 20, color: Constants.kitGradients[10]))
                  : Icon(Icons.add, size: 20, color: Colors.grey),
            ],
          ),
        ],
      ),
    );
  }
}
