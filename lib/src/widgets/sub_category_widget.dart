import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SubCategoryWidget extends StatefulWidget {
  final String images;
  final int currentIndex;
  final Function onPressed;
  SubCategoryWidget({this.images, this.currentIndex, this.onPressed});

  @override
  _SubCategoryWidgetState createState() => _SubCategoryWidgetState();
}

class _SubCategoryWidgetState extends State<SubCategoryWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: Container(
              height: screenHeight(context, dividedBy: 1),
              width: screenWidth(context, dividedBy: 1),
              decoration: BoxDecoration(
                  backgroundBlendMode: BlendMode.colorBurn,
                  image: DecorationImage(
                    image: AssetImage(
                      widget.images,
                    ),
                    fit: BoxFit.fill,
                    colorFilter: new ColorFilter.mode(
                        Colors.black.withOpacity(0.6), BlendMode.darken),
                  ),
                  color: Constants.kitGradients[5]),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                child: Text(
                  "Hot Drinks",
                  style: TextStyle(
                      color: Constants.kitGradients[0].withOpacity(0.7),
                      fontSize: 20,
                      fontFamily: "OswaldRegular",
                      fontWeight: FontWeight.w800),
                ),
              ),

              // Row(
              //   children: [
              //     SizedBox(
              //       width: screenWidth(context, dividedBy: 20),
              //     ),
              //     Container(
              //       height: screenHeight(context, dividedBy: 20),
              //       decoration: BoxDecoration(
              //           color: Constants.kitGradients[6],
              //           shape: BoxShape.circle),
              //       child: Center(
              //           child: Padding(
              //         padding: EdgeInsets.all(8.0),
              //         child: Icon(
              //           Icons.double_arrow,
              //           color: Colors.white,
              //         ),
              //       )),
              //     ),
              //   ],
              // ),
            ],
          ),
        ],
      ),
    );
  }
}
