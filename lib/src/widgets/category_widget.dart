import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CategoryWidget extends StatefulWidget {
  final String images;
  final int currentIndex;
  final Function onPressed;
  CategoryWidget({this.images, this.currentIndex, this.onPressed});

  @override
  _CategoryWidgetState createState() => _CategoryWidgetState();
}

class _CategoryWidgetState extends State<CategoryWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: Container(
              child: Image.asset(
                widget.images,
                fit: BoxFit.fill,
                height: screenHeight(context, dividedBy: 1),
                width: screenWidth(context, dividedBy: 2),
              ),
            ),
          ),
          Column(
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 20),
              ),
              Row(
                children: [
                  SizedBox(
                    width: screenWidth(context, dividedBy: 20),
                  ),
                  Container(
                      width: screenWidth(context, dividedBy: 4),
                      child: Text(
                        "Hot Drinks",
                        style: TextStyle(
                            color: Constants.kitGradients[2],
                            fontSize: 20,
                            fontFamily: "OswaldRegular",
                            fontWeight: FontWeight.w800),
                      )),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 50),
              ),
              Row(
                children: [
                  SizedBox(
                    width: screenWidth(context, dividedBy: 20),
                  ),
                  Container(
                    height: screenHeight(context, dividedBy: 20),
                    decoration: BoxDecoration(
                        color: Constants.kitGradients[6],
                        shape: BoxShape.circle),
                    child: Center(
                        child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Icon(
                        Icons.double_arrow,
                        color: Colors.white,
                      ),
                    )),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
