import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';

class HeadingWidget extends StatefulWidget {
  String title;
  HeadingWidget({this.title});
  @override
  _HeadingWidgetState createState() => _HeadingWidgetState();
}

class _HeadingWidgetState extends State<HeadingWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          top: screenHeight(context, dividedBy: 80),
          bottom: screenHeight(context, dividedBy: 50)),
      child: Text(
        widget.title,
        style: TextStyle(
          color: Constants.kitGradients[2],
          fontWeight: FontWeight.w400,
          fontSize: 23,
          fontFamily: "PlayFairRegular",
        ),
      ),
    );
  }
}
