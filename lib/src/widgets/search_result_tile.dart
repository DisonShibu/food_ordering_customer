import 'package:app_template/src/screens/product_detail_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/add_button.dart';
import 'package:app_template/src/widgets/build_icon_button.dart';
import 'package:app_template/src/widgets/star_rating_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SearchResultTile extends StatefulWidget {
  final String restarauntName;
  final String foodName;
  final String price;
  final String images;
  final Function onPressed;
  final Function onPressedAdd;
  final String rating;
  SearchResultTile(
      {this.images,
      this.foodName,
      this.price,
      this.restarauntName,
      this.onPressedAdd,
      this.rating,
      this.onPressed});
  @override
  _SearchResultTileState createState() => _SearchResultTileState();
}

class _SearchResultTileState extends State<SearchResultTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        push(context, ProductDetailPage());
      },
      child: Card(
        child: Container(
          height: screenHeight(context, dividedBy: 4),
          width: screenWidth(context, dividedBy: 1),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 50)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 40),
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 10),
                        ),
                        Container(
                          width: screenWidth(context, dividedBy: 2),
                          child: Text(
                            widget.restarauntName,
                            style: TextStyle(
                                color: Constants.kitGradients[10],
                                fontSize: screenWidth(context, dividedBy: 23),
                                fontFamily: "OswaldRegular"),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 100),
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.radio_button_checked,
                          size: 20,
                          color: Colors.red,
                        ),
                        SizedBox(
                          width: screenWidth(context, dividedBy: 20),
                        ),
                        Container(
                          width: screenWidth(context, dividedBy: 2),
                          child: Text(
                            widget.foodName,
                            style: TextStyle(
                                color: Constants.kitGradients[5],
                                fontSize: screenWidth(context, dividedBy: 20),
                                fontFamily: "PrompLight"),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 10),
                        ),
                        Container(
                          width: screenWidth(context, dividedBy: 2),
                          child: Text(
                            widget.price + " SR",
                            style: TextStyle(
                                color: Constants.kitGradients[5],
                                fontSize: screenWidth(context, dividedBy: 23),
                                fontFamily: "PrompLight"),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 10),
                        ),
                        Text(
                          widget.rating,
                          style: TextStyle(
                              color: Constants.kitGradients[5],
                              fontSize: screenWidth(context, dividedBy: 23),
                              fontFamily: "PrompLight"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        StarRatingBar(
                          rating: 3.5,
                          color: Constants.kitGradients[2],
                          starCount: 5,
                        )
                      ],
                    ),
                  ],
                ),
              ),
              Row(
                children: [
                  Center(
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Container(
                          width: screenWidth(context, dividedBy: 4.5),
                          height: screenHeight(context, dividedBy: 9),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Image.asset(
                              widget.images,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        Column(
                          children: [
                            SizedBox(
                              height: screenHeight(context, dividedBy: 6.4),
                            ),
                            AddButton(
                              onPressed: () {
                                widget.onPressedAdd();
                              },
                              title: "ADD",
                              icon: "assets/icons/plus.svg",
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
