import 'dart:ui';

import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SearchBar extends StatefulWidget {
  final String title;
  final IconData icon;
  final TextEditingController searchTextEditingController;
  final Function onPressed;
  SearchBar(
      {this.searchTextEditingController,
      this.title,
      this.icon,
      this.onPressed});

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 30,
      color: Constants.kitGradients[0].withOpacity(0.4),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.1),
            boxShadow: [
              BoxShadow(
                  color: Constants.kitGradients[2].withOpacity(0.1),
                  spreadRadius: 2.0,
                  offset: Offset(0.4, 0.4),
                  blurRadius: 2.0)
            ],
            borderRadius: BorderRadius.circular(5)),
        child: TextField(
          onTap: widget.onPressed,
          cursorColor: Colors.black45,
          keyboardType: TextInputType.text,
          controller: widget.searchTextEditingController,
          selectionHeightStyle: BoxHeightStyle.tight,
          decoration: InputDecoration(
              fillColor: Colors.grey,
              contentPadding: const EdgeInsets.symmetric(vertical: 0),
              prefixIcon: Icon(
                widget.icon,
                size: 30,
                color: Constants.kitGradients[0].withOpacity(0.6),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                    color: Constants.kitGradients[1].withOpacity(0.1),
                    width: 2.0),
                borderRadius: BorderRadius.circular(10),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                    color: Constants.kitGradients[2].withOpacity(0.1),
                    width: 2.0),
                borderRadius: BorderRadius.circular(10),
              ),
              hintText: widget.title,
              hintStyle: TextStyle(
                  color: Constants.kitGradients[0], fontFamily: "PrompLight"),
              border: InputBorder.none),
        ),
      ),
    );
  }
}
