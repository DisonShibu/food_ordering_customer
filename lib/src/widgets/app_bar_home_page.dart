import 'package:app_template/src/screens/saved_location_page.dart';
import 'package:app_template/src/screens/search_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/location_selection_box.dart';
import 'package:app_template/src/widgets/location_text_box.dart';
import 'package:app_template/src/widgets/search_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppBarHomePage extends StatefulWidget {
  final String title;
  final TextEditingController searchTextEditingController;
  final Function onPressedLeft;
  final Function onPressedRight;
  final Function onPressedLocation;
  final String location;
  AppBarHomePage(
      {this.searchTextEditingController,
      this.title,
      this.onPressedLeft,
      this.onPressedLocation,
      this.location,
      this.onPressedRight});

  @override
  _AppBarHomePageState createState() => _AppBarHomePageState();
}

class _AppBarHomePageState extends State<AppBarHomePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 4.5),
      decoration: BoxDecoration(
        color: Constants.kitGradients[2],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: screenHeight(context, dividedBy: 50),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Spacer(
                  flex: 1,
                ),
                Spacer(
                  flex: 1,
                ),
                GestureDetector(
                    onTap: () {
                      widget.onPressedLeft();
                    },
                    child: Icon(
                      Icons.menu_rounded,
                      color: Colors.white,
                      size: 30,
                    )),
                Spacer(
                  flex: 10,
                ),
                Text(
                  "Food Delivery App",
                  style:
                      TextStyle(color: Constants.kitGradients[0], fontSize: 19),
                ),
                Spacer(
                  flex: 10,
                ),
                GestureDetector(
                    onTap: () {
                      widget.onPressedRight();
                    },
                    child: Icon(
                      Icons.notifications,
                      color: Colors.white,
                      size: 30,
                    )),
                Spacer(
                  flex: 1,
                ),
              ],
            ),
          ),
          Spacer(
            flex: 5,
          ),
          Row(
            children: [
              LocationTextBox(
                onPressed: () {
                  push(context, SavedLocationPage());
                },
                icon: Icons.location_on_outlined,
                padding: 20,
                dividerTrue: false,
                heading: widget.location,
                boxWidth: 1.3,
                colorWhite: true,
              ),
              // Spacer(flex: 2),
              // Container(
              //   width: screenWidth(context, dividedBy: 20),
              //   child: Icon(
              //     Icons.search,
              //     color: Colors.black54,
              //     size: screenWidth(context, dividedBy: 14),
              //   ),
              // ),
              // Spacer(
              //   flex: 1,
              // )
            ],
          ),
          Spacer(
            flex: 3,
          )
        ],
      ),
    );
  }
}
