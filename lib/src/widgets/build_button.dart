import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BuildButton extends StatefulWidget {
  final String title;
  final Function onPressed;
  BuildButton({this.title, this.onPressed});

  @override
  _BuildButtonState createState() => _BuildButtonState();
}

class _BuildButtonState extends State<BuildButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 2),
      height: screenHeight(context, dividedBy: 20),
      decoration: BoxDecoration(
        color: Colors.transparent,

        // boxShadow: [
        //
        //   BoxShadow(
        //       color:Constants.kitGradients[4],
        //       spreadRadius: 3,
        //       offset: Offset(0,3,),
        //   blurRadius: 5.0,)
        //
        // ]
      ),
      child: ElevatedButton(
        onPressed: () {
          widget.onPressed();
        },
        style: ElevatedButton.styleFrom(
          primary: Constants.kitGradients[6],
          onPrimary: Constants.kitGradients[6],
          shadowColor: Constants.kitGradients[2],
          elevation: 9,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
        ),
        child: Text(
          widget.title,
          style: TextStyle(
              color: Constants.kitGradients[0],
              fontFamily: 'PromptLight',
              fontSize: 16),
        ),
      ),
    );
  }
}
