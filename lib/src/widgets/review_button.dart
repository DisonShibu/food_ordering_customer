import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ReviewButton extends StatefulWidget {
  final String title;
  final Function onPressed;
  ReviewButton({this.title, this.onPressed});

  @override
  _ReviewButtonState createState() => _ReviewButtonState();
}

class _ReviewButtonState extends State<ReviewButton> {
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        widget.onPressed();
      },
      style: ElevatedButton.styleFrom(
        primary: Colors.white,
        onPrimary: Colors.white,
        elevation: 0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(7),
            side: BorderSide(color: Constants.kitGradients[10], width: 1)),
      ),
      child: Text(
        widget.title,
        style: TextStyle(
            color: Constants.kitGradients[10],
            fontFamily: 'PromptLight',
            fontSize: 16),
      ),
    );
  }
}
