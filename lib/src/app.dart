import 'package:app_template/localisation/demo_local.dart';
import 'package:app_template/src/screens/location_fetching_page.dart';
import 'package:app_template/src/screens/login_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:app_template/src/utils/hive.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  static void setLocale(BuildContext context, Locale locale) {
    _MyAppState state = context.findAncestorStateOfType<_MyAppState>();
    state.setLocale(locale);
    // MyApp.setLocale(context, locality);
  }

  @override
  _MyAppState createState() => _MyAppState();
}

String lc;
String cc;
Locale _locale;
Locale locality;

class _MyAppState extends State<MyApp> {
  void setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  void initState() {
    setState(() {
      lc = AppHive().getLanguage();
      cc = AppHive().getLanguage();
    });

    super.initState();
  }

  @override
  void didChangeDependencies() {
    setState(() {
      // _locale = Locale(lc, cc) ?? Locale('en', 'US');
      if ((lc != null) && (cc != null)) {
        _locale = Locale(lc);
      } else {
        _locale = Locale('en');
      }
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blue,
          textTheme: Theme.of(context).textTheme.apply(
              fontFamily: 'Nexa',
              bodyColor: Constants.kitGradients[0],
              displayColor: Constants.kitGradients[0]),
          // This makes the visual density adapt to the platform that you run
          // the app on. For desktop platforms, the controls will be smaller and
          // closer together (more dense) than on mobile platforms.
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        locale: _locale,
        localizationsDelegates: [
          DemoLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        localeResolutionCallback: (deviceLocale, supportedLocales) {
          for (var locale in supportedLocales) {
            if (locale.languageCode == deviceLocale.languageCode &&
                locale.countryCode == deviceLocale.countryCode) {
              return deviceLocale;
            }
          }
          return supportedLocales.first;
        },
        supportedLocales: [
          Locale('en'), // English
          Locale('ar'), //Arabic
        ],
        home:LoginPage());
  }

// void didChangeDependencies() {
//   setState(() {
//     this._locale = locality;
//   });
//   print(locality.languageCode);
//   super.didChangeDependencies();
// }

}
