import 'package:app_template/src/resources/api_providers/user_api_provider.dart';
import 'package:app_template/src/resources/repository/repository.dart';
import 'package:app_template/src/utils/api_client.dart';

import 'dio.dart';
import 'hive.dart';

/// it is a hub that connecting pref,repo,client
/// used to reduce imports in pages
class ObjectFactory {
  static final _objectFactory = ObjectFactory._internal();

  ObjectFactory._internal();

  factory ObjectFactory() => _objectFactory;

  ///Initialisation of Objects
  AppHive _appHive = AppHive();
  AppDio _appDio = AppDio();
  ApiClient apiClient = ApiClient();
  Repository repository = Repository();
  UserApiProvider userApiProvider = UserApiProvider();

  ///
  /// Getters of Objects
  ///
  AppDio get appDio => _appDio;

  AppHive get appHive => _appHive;

  ///
  /// Setters of Objects
  ///

}
