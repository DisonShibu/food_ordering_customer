import 'dart:io';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/widgets/cancel_alert_box.dart';
import 'package:app_template/src/widgets/write_review_alert_box.dart';
import 'package:app_template/src/widgets/otp_message.dart';
import 'package:cherry_toast/cherry_toast.dart';
import 'package:cherry_toast/resources/arrays.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

///it contain common functions
class Utils {}

Size screenSize(BuildContext context) {
  return MediaQuery.of(context).size;
}

double screenHeight(BuildContext context, {double dividedBy = 1}) {
  return screenSize(context).height / dividedBy;
}

double screenWidth(BuildContext context, {double dividedBy = 1}) {
  return screenSize(context).width / dividedBy;
}

Future<dynamic> push(BuildContext context, Widget route) {
  return Navigator.push(
      context, MaterialPageRoute(builder: (context) => route));
}

void pop(BuildContext context) {
  return Navigator.pop(context);
}

Future<dynamic> pushAndRemoveUntil(
    BuildContext context, Widget route, bool goBack) {
  return Navigator.pushAndRemoveUntil(context,
      MaterialPageRoute(builder: (context) => route), (route) => goBack);
}

Future<dynamic> pushAndReplacement(BuildContext context, Widget route) {
  return Navigator.pushReplacement(
      context, MaterialPageRoute(builder: (context) => route));
}

///common toast
void showToast(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM,
  );
}

void reviewDialog(
    {context,
    msg,
    double insetPadding,
    double contentPadding,
    double titlePadding,
    Function onPressed,
    reviewTexteditingController}) {
  showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.black.withOpacity(0.8),
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext buildContext, Animation animation,
          Animation secondaryAnimation) {
        return WriteReviewAlertBox(
          title: msg,
          contentPadding: contentPadding,
          reviewTextEditingController: reviewTexteditingController,
          titlePadding: titlePadding,
          insetPadding: insetPadding,
          onPressed: onPressed,
        );
      });
}

void cancelAlertBoxFun({
  context,
  String msg,
  String title,
  double insetPadding,
  double contentPadding,
  double titlePadding,
  Function onPressedYes,
  Function onPressedNo,
}) {
  showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.black.withOpacity(0.8),
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext buildContext, Animation animation,
          Animation secondaryAnimation) {
        return CancelAlertBox(
          title: msg,
          contentPadding: contentPadding,
          titlePadding: titlePadding,
          insetPadding: insetPadding,
          onPressedNo: onPressedNo,
          onPressedYes: onPressedYes,
        );
      });
}

void showAlert(context, String msg) {
  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: new Text(msg),
//        content: new Text("Alert Dialog body"),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("OK"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

void otpAlertBox({context, title, route, stayOnPage}) {
  showGeneralDialog(
      context: context,
      barrierDismissible: false,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.transparent,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext buildContext, Animation animation,
          Animation secondaryAnimation) {
        return OtpMessage(
          title: title,
          route: route,
          stayOnPage: stayOnPage,
        );
      });
}

///common toast
void showToastLong(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
  );
}

void showToastConnection(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
  );
}

void showToastInfo(String message, BuildContext context) {
  CherryToast(
          icon: Icons.info,
          iconSize: 40,
          themeColor: Constants.kitGradients[0],
          title: "",
          displayTitle: true,
          description: message,
          descriptionStyle: TextStyle(
            color: Colors.red,
          ),
          toastPosition: POSITION.BOTTOM,
          animationDuration: Duration(milliseconds: 1000),
          autoDismiss: true)
      .show(context);
}

void showToastError(String message, BuildContext context) {
  CherryToast(
          icon: Icons.error,
          iconSize: 40,
          themeColor: Constants.kitGradients[0],
          title: "",
          displayTitle: false,
          description: message,
          toastPosition: POSITION.BOTTOM,
          animationDuration: Duration(milliseconds: 1000),
          autoDismiss: true)
      .show(context);
}

void showToastSuccess(String message, BuildContext context) {
  CherryToast(
          icon: Icons.check_circle,
          iconSize: 40,
          themeColor: Constants.kitGradients[0],
          title: "",
          displayTitle: false,
          description: message,
          toastPosition: POSITION.BOTTOM,
          animationDuration: Duration(milliseconds: 1000),
          autoDismiss: true)
      .show(context);
}
