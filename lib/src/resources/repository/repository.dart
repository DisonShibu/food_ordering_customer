import 'package:app_template/src/models/item_model.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/login_response_model.dart';
import 'package:app_template/src/resources/api_providers/user_api_provider.dart';
import 'package:app_template/src/resources/api_response.dart';

/// Repository is an intermediary class between network and data
class Repository {
  final userApiProvider = UserApiProvider();

  Future<ItemModel> login() => UserApiProvider().itemModelCall();
}
