import 'dart:convert';
import 'dart:io';

import 'package:app_template/src/models/item_model.dart';
import 'package:app_template/src/utils/exception.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:dio/dio.dart';

class UserApiProvider {
  Future<ItemModel> itemModelCall() async {
    print("UserApi Provider");
    try {
      final response = await ObjectFactory().apiClient.itemModelApiClient();
      print("UserApiProviderCallBack");
      print(response);

      return ItemModel.fromJson(response.data);
      // responseJson = _returnResponse(response);
    } on DioError catch (e) {
      print("New exception");
      throw FetchDataException('No Internet connection');
    }
  }
}
