import 'package:app_template/src/models/item_model.dart';
import 'package:app_template/src/resources/api_response.dart';
import 'dart:io';
import 'dart:async';

import 'package:app_template/src/utils/object_factory.dart';

class UserBloc {
  StreamController<ApiResponse<ItemModel>> _movieListController =
      new StreamController<ApiResponse<ItemModel>>.broadcast();

  StreamSink<ApiResponse<ItemModel>> get movieListSink =>
      _movieListController.sink;

  Stream<ApiResponse<ItemModel>> get movieListStream =>
      _movieListController.stream;

  fetchItemModel() async {
    movieListSink.add(ApiResponse.loading('Fetching Popular Movies'));
    try {
      ItemModel movies = await ObjectFactory().repository.login();
      print("Success");
      movieListSink.add(ApiResponse.completed(movies));
    } catch (e) {
      print("Block Call Back");
      print("exception" + e.toString());
      movieListSink.add(ApiResponse.error(e.toString()));
      print(e);
    }
  }

  dispose() {
    _movieListController?.close();
  }
}
