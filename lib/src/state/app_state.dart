import 'package:app_template/src/models/hive_db_models/cart_page_hive_models.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:hive/hive.dart';

enum AppStartUp {
  YES,
  NO,
}

class HomeState with ChangeNotifier {
  AppStartUp isAppStartUp = AppStartUp.NO;
  String address;
  Position currentLocation;
  String foodName;

  void locationStatus({bool value}) {
    if (value == true) {
      isAppStartUp = AppStartUp.YES;
    } else {
      isAppStartUp = AppStartUp.NO;
    }

    notifyListeners();
  }

  void locationValueStored({String value, Position locationCoordinates}) {
    address = value;
    currentLocation = locationCoordinates;

    notifyListeners();
  }

  Future deleteItem(CartItems cartItems) async {}

  Future<bool> checkItem(int id) async {
    var cartItemBox = await Hive.openBox<CartItems>(Constants.ADAPTERBOX_NAME);

    bool isCartItem = cartItemBox.get(id) == null ? true : false;
    print(isCartItem);
    if (isCartItem == false)
      foodName = cartItemBox.get(id).results.name == null
          ? null
          : cartItemBox.get(id).results.name;
    notifyListeners();
    return isCartItem;
  }

  Future addItem(CartItems cartItem) async {
    var cartItemBox = await Hive.openBox<CartItems>(Constants.ADAPTERBOX_NAME);

    print(cartItemBox.values);
    print("AddedItems to the Box" + cartItem.results.name);
    print("Item Id" + cartItem.results.id.toString());
    cartItemBox.put(cartItem.results.id, cartItem);

    notifyListeners();
  }

  Future<bool> checkSameRestaurant(String restaurantId) async {
    bool isRestaurantSame = true;
    var cartItemBox = await Hive.openBox<CartItems>(Constants.ADAPTERBOX_NAME);

    if (cartItemBox.isNotEmpty) {
      if ("0" == restaurantId) {
        print("SameRestaurant");
        isRestaurantSame = true;
      } else {
        isRestaurantSame = false;
      }
    }
    notifyListeners();

    return isRestaurantSame;
  }

  Future clearHive() async {
    bool isRestaurantSame = true;
    var cartItemBox = await Hive.openBox<CartItems>(Constants.ADAPTERBOX_NAME);
    cartItemBox.clear();
    notifyListeners();
  }

  Future clearHiveItem(String id) async {
    bool isRestaurantSame = true;
    var cartItemBox = await Hive.openBox<CartItems>(Constants.ADAPTERBOX_NAME);
    cartItemBox.delete(id);
    notifyListeners();
  }
}
