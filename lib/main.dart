import 'package:app_template/src/app.dart';
import 'package:app_template/src/models/hive_db_models/cart_page_hive_models.dart';
import 'package:app_template/src/state/app_state.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:hive/hive.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDocDir = await getApplicationDocumentsDirectory();
  Hive.init(appDocDir.path);
  Hive.registerAdapter(CartItemsAdapter());
  await Hive.openBox<CartItems>(Constants.ADAPTERBOX_NAME);
  await Hive.openBox(Constants.BOX_NAME);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Constants.kitGradients[2],
  ));
  runApp(MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => HomeState())],
      child: MyApp()));
}
